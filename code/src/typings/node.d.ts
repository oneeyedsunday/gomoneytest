declare namespace NodeJS {
  export interface ProcessEnv {
    PORT: number | string;
    REDIS_PASSWORD: string;
    MONGO_USERNAME: string;
    MONGO_PASSWORD: string;
    IS_PRODUCTION: "true" | "false";
    API_URL: string;
    REDIS_PORT?: number;
    REDIS_URL?: string;
    REDIS_CONN_STRING?: string;
    REDIS_PASSWORD?: string;
    JWT_SECRET: string;
    JWT_TOKEN_EXPIRE_IN_SEC: number;
    FIREBASE_SERVICE_ACCOUNT_PATH: string;
    MONGO_URL: string;
    MONGO_DBNAME: string;
    LINK_GEN_BASE_URL: string;
    RATE_LIMIT_MINS: number;
    RATE_LIMIT_MAX_REQ: number;
    CACHE_TTL_SECONDS: number;
  }
}
