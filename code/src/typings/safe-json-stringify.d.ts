declare module "@bugsnag/safe-json-stringify" {
  export default function (item: any): string;
}
