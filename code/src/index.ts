import * as dotenv from "dotenv";

dotenv.config();

import "./shared/infrastructure/http/app";
import "./shared/infrastructure/database/mongo";
import "./modules/search/subscriptions";
