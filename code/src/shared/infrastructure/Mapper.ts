export interface Mapper<T, U> {
  toDomain(raw: any): T;
  toDTO(from: T): U;
  toDTOFromSerialized(raw: any): U;
}
