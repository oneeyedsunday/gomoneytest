import { Logger } from "./ILogger";

const logger = console;

export class ConsoleLogger implements Logger {
  error(...args: any[]): void {
    logger.error(...args);
  }

  info(...args: any[]): void {
    logger.info(...args);
  }

  trace(...args: any[]): void {
    logger.trace(...args);
  }

  debug(...args: any[]): void {
    logger.debug(...args);
  }
}
