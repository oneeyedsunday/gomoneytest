import { defaultMongoOpts } from "@random-guys/bucket";
import { LoggerImpl } from "@shared/infrastructure/logger";
import mongoose from "mongoose";

mongoose.set("debug", function (coll: string, method: string, query: any) {
  LoggerImpl.info(
    `[MongoQuery:] "${method}" on "${coll}" with query ${JSON.stringify(query)}`
  );
});

const connection = mongoose.createConnection(process.env.MONGO_URL as string, {
  ...defaultMongoOpts,
  numberOfRetries: 3,
  connectTimeoutMS: 5000,
});

connection.once("open", () => {
  LoggerImpl.info(`[MongoDB]: Connected to mongoDB server`);
});

export { connection };
