import { DomainEvents } from "@shared/domain/events/DomainEvents";
import { UniqueEntityID } from "@shared/domain/UniqueEntityID";
import { ObjectID } from "mongodb";
import { Document, Schema } from "mongoose";

type PayloadTransformer<T, U> = (
  document: Document<T>,
  options?: Promise<U>
) => any;

type hookOptions<T, U> = {
  save?: PayloadTransformer<T, U>;
};

function dispatchEventsCallback<T, U>(
  model: Document,
  transformer?: PayloadTransformer<T, U>
): void {
  const options = transformer ? transformer(model) : null;
  DomainEvents.dispatchEventsForAggregate(
    new UniqueEntityID(new ObjectID(model._id)),
    options
  );
}

export function runHook<T = any, U = any>(
  schema: Schema,
  options?: hookOptions<T, U>
): void {
  schema.post(["save", "updateOne"], (document) =>
    dispatchEventsCallback(document, options?.save)
  );

  schema.post("findOneAndDelete", (document) =>
    dispatchEventsCallback(document)
  );
}
