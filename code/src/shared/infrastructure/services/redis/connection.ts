import redis from "redis";
import { RedisClient } from "redis";
import { redisConfig, isProduction } from "@config";
import { LoggerImpl } from "@shared/infrastructure/logger";

const port: number = redisConfig.redisServerPort;
const host = redisConfig.redisServerURL;
const redisConnection: RedisClient = isProduction
  ? redis.createClient(redisConfig.redisConnectionString as string)
  : redis.createClient(port, host, { password: redisConfig.redisPassword });

redisConnection.on("connect", () => {
  LoggerImpl.info(`[Redis]: Connected to redis server at ${host}:${port}`);
});

export { redisConnection };
