import { RedisClient } from "redis";
import { WriteAsideCache } from "./Cache";

export abstract class RedisWriteAsideCache<DataT, SeekT>
  implements WriteAsideCache<DataT, SeekT>
{
  protected readonly keyFactory: (options: SeekT) => string;
  protected readonly client: RedisClient;
  protected readonly ttl: number;
  constructor(
    redisClient: RedisClient,
    ttl: number,
    keyFactory: (options: SeekT) => string
  ) {
    this.ttl = ttl;
    this.client = redisClient;
    this.keyFactory = keyFactory;
  }

  abstract getData(options: SeekT): Promise<DataT | null>;
  abstract setData(value: DataT, options: SeekT): Promise<void>;

  protected getAsync(key: string): Promise<string | null> {
    return new Promise((resolve, reject) => {
      this.client.get(key, (error: Error | null, reply) => {
        if (error) {
          return reject(error);
        } else {
          return resolve(reply);
        }
      });
    });
  }

  protected setAsync(key: string, data: string): Promise<void> {
    // TODO (oneeyedsunday) set data in background always
    return new Promise((resolve, reject) => {
      this.client
        .multi()
        .set(key, data)
        .expire(key, this.ttl)
        .exec((err) => {
          if (err) reject(err);
          resolve();
        });
    });
  }
}
