export interface WriteAsideCache<T, Q> {
  getData(options: Q): Promise<T | null>;
  setData(value: T, options: Q): Promise<void>;
}
