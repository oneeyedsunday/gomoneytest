import express from "express";
import { json } from "body-parser";
import { catchRouter } from "@shared/infrastructure/http/api/catch";
import { router } from "../http/api/routes";

// Returns handler for server
function bootstrapApp(): express.Express {
  const app = express();

  app.use(json());

  app.use("/api", router);

  // 404 handler
  app.use(catchRouter);
  return app;
}

export { bootstrapApp };
