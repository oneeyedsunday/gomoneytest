import { authService } from "@modules/users/services";
import { AuthMiddleware } from "./middlewares/Auth";
import { AdminMiddleware } from "./middlewares/Admin";
import { RateLimitMiddleware } from "./middlewares/RateLimit";

const authMiddleware = new AuthMiddleware(authService);
const adminMiddleware = new AdminMiddleware(authService);
const rateLimitMiddleware = new RateLimitMiddleware(authService);

export { authMiddleware, adminMiddleware, rateLimitMiddleware };
