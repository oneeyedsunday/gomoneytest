import { RateLimitStore } from "./store";
import BaseRedisStore from "rate-limit-redis";
import { redisConnection } from "@shared/infrastructure/services/redis/connection";

export class RedisRateLimitStore
  extends BaseRedisStore
  implements RateLimitStore
{
  constructor(expiryMs: number) {
    const options = {
      expiry: expiryMs / 1000,
      client: redisConnection,
      prefix: "rate-limit:",
      passIfNotConnected: true,
    };
    super(options);
  }
}
