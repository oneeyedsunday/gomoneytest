import rateLimit from "express-rate-limit";

export type RateLimitStore = rateLimit.Store;
