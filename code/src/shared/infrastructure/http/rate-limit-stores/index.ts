import { RedisRateLimitStore } from "./redisStore";
import { RateLimitStore } from "./store";

const storeFactory = (expiryMs: number): RateLimitStore =>
  new RedisRateLimitStore(expiryMs);

export { storeFactory };
