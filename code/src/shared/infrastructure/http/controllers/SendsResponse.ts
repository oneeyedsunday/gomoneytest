import * as express from "express";

export class SendsResponse {
  public static jsonResponse(
    res: express.Response,
    code: number,
    message: string
  ) {
    const isError = code >= 400;
    if (isError)
      return res.status(code).json({ status: "error", message, code });
    return res.status(code).json({ message, status: "success" });
  }

  public ok<T>(res: express.Response, dto?: T) {
    if (dto) {
      res.type("application/json");
      return res.status(200).json({ status: "success", data: dto });
    } else {
      return res.status(201).json({ status: "success" });
    }
  }

  public created(res: express.Response) {
    return res.sendStatus(201);
  }

  public clientError(res: express.Response, message?: string) {
    return SendsResponse.jsonResponse(
      res,
      400,
      message ? message : "Unauthorized"
    );
  }

  public unauthorized(res: express.Response, message?: string) {
    return SendsResponse.jsonResponse(
      res,
      401,
      message ? message : "Unauthorized"
    );
  }

  public paymentRequired(res: express.Response, message?: string) {
    return SendsResponse.jsonResponse(
      res,
      402,
      message ? message : "Payment required"
    );
  }

  public forbidden(res: express.Response, message?: string) {
    return SendsResponse.jsonResponse(
      res,
      403,
      message ? message : "Forbidden"
    );
  }

  public notFound(res: express.Response, message?: string) {
    return SendsResponse.jsonResponse(
      res,
      404,
      message ? message : "Not found"
    );
  }

  public conflict(res: express.Response, message?: string) {
    return SendsResponse.jsonResponse(res, 409, message ? message : "Conflict");
  }

  public validationFailed(res: express.Response, message?: string) {
    return SendsResponse.jsonResponse(
      res,
      422,
      message ? message : "Unprocessable Entity"
    );
  }

  public tooMany(res: express.Response, message?: string) {
    return SendsResponse.jsonResponse(
      res,
      429,
      message ? message : "Too many requests"
    );
  }
}
