import { IAuthService } from "@modules/users/services/authService";
import * as express from "express";
import { SendsResponse } from "../controllers/SendsResponse";

export abstract class Middleware extends SendsResponse {
  protected authService: IAuthService;

  constructor(authService: IAuthService) {
    super();
    this.authService = authService;
  }

  protected endRequest(
    status: 401 | 403 | 429,
    message: string,
    res: express.Response
  ): any {
    switch (status) {
      case 401:
        return this.unauthorized(res, message);
      case 403:
        return this.forbidden(res, message);
      default:
        return this.clientError(res, message);
    }
  }
}
