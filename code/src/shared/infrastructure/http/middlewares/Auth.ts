import * as express from "express";
import { IAuthService } from "@modules/users/services/authService";
import { Middleware } from "./Base";
import { DecodedExpressRequest } from "@shared/infrastructure/http/models/decodedExpressRequest";
import { JWTClaims } from "@shared/domain/jwt";

export class AuthMiddleware extends Middleware {
  constructor(authService: IAuthService) {
    super(authService);
  }

  public ensureAuthenticated() {
    return async (
      req: express.Request,
      res: express.Response,
      next: express.NextFunction
    ) => {
      const bearerToken = req.headers["authorization"];
      if (!bearerToken)
        return this.endRequest(401, "No access token provided", res);
      const [_, token] = bearerToken?.split(" ");
      // Confirm that the token was signed with our signature.
      if (token) {
        let decoded: JWTClaims;
        try {
          decoded = await this.authService.decodeJWT(token);
        } catch (err) {
          return this.endRequest(401, err.message, res);
        }

        const signatureFailed = !decoded;

        if (signatureFailed) {
          return this.endRequest(401, "Token signature expired.", res);
        }

        // See if the token was found
        const { username } = decoded;
        const tokens = await this.authService.getTokens(username);

        // if the token was found, just continue the request.
        if (tokens.length !== 0) {
          (req as DecodedExpressRequest).decoded = decoded;
          return next();
        } else {
          return this.endRequest(
            401,
            "Auth token not found. User is probably not logged in. Please login again.",
            res
          );
        }
      } else {
        return this.endRequest(401, "No access token provided", res);
      }
    };
  }
}
