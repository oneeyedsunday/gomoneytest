import rateLimit from "express-rate-limit";
import { IAuthService } from "@modules/users/services/authService";
import { Middleware } from "./Base";
import express from "express";
import { DecodedExpressRequest } from "@shared/infrastructure/http/models/decodedExpressRequest";
import { storeFactory } from "../rate-limit-stores";

export class RateLimitMiddleware extends Middleware {
  constructor(authService: IAuthService) {
    super(authService);
  }

  private static createRateLimit(
    mins: number,
    maxRequests: number,
    instance: Middleware
  ) {
    return rateLimit({
      windowMs: mins * 60 * 1000,
      max: maxRequests,
      store: storeFactory(mins * 60 * 1000),
      skipFailedRequests: true,
      keyGenerator: (req: DecodedExpressRequest) =>
        req.decoded ? req.decoded.userId : req.ip,
      handler: (req: express.Request, res: express.Response) =>
        instance.tooMany(
          res,
          `Too Many Requests. Next window epoch: ${new Date(
            req.rateLimit.resetTime as Date
          ).valueOf()}`
        ),
    });
  }

  public defaultLimits(): rateLimit.RateLimit {
    const mins = Number(process.env.RATE_LIMIT_MINS || 5);
    const maxRequests = Number(process.env.RATE_LIMIT_MAX_REQ || 5);
    return RateLimitMiddleware.createRateLimit(mins, maxRequests, this);
  }
}
