import * as express from "express";
import { DecodedExpressRequest } from "@shared/infrastructure/http/models/decodedExpressRequest";
import { IAuthService } from "@modules/users/services/authService";
import { Middleware } from "./Base";

export class AdminMiddleware extends Middleware {
  constructor(authService: IAuthService) {
    super(authService);
  }

  public ensureIsAdmin() {
    return async (
      req: express.Request,
      res: express.Response,
      next: express.NextFunction
    ) => {
      const { decoded } = req as DecodedExpressRequest;

      if (!decoded?.isAdmin) return this.endRequest(403, "Forbidden.", res);

      return next();
    };
  }
}
