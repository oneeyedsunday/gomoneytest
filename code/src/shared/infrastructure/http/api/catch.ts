import express from "express";
import { BaseController } from "@shared/infrastructure/http/controllers/Base";

const catchRouter = express.Router();

class Controller extends BaseController {
  async executeImpl(
    req: express.Request,
    res: express.Response
  ): Promise<void | any> {
    return this.notFound(res, `Resource ${req.originalUrl} not found`);
  }
}

const controller = new Controller();

catchRouter.use((req, res) => controller.execute(req, res));

export { catchRouter };
