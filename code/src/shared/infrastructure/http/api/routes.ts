import express from "express";
import { userRouter } from "@modules/users/infra/http/routes";
import { teamRouter } from "@modules/teams/infra/http/routes";
import { fixtureRouter } from "@modules/fixtures/infra/http/routes";
import { searchRouter } from "@modules/search/infra/http/routes";
import {
  authMiddleware,
  rateLimitMiddleware,
} from "@shared/infrastructure/http";

const router = express.Router();

router.use("/auth", userRouter);
router.use(
  "/fixtures",
  authMiddleware.ensureAuthenticated(),
  rateLimitMiddleware.defaultLimits(),
  fixtureRouter
);
router.use(
  "/teams",
  authMiddleware.ensureAuthenticated(),
  rateLimitMiddleware.defaultLimits(),
  teamRouter
);

router.use("/search", searchRouter);

export { router };
