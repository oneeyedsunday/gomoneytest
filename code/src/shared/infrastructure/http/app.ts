import morgan from "morgan";
import cors from "cors";
import helmet from "helmet";
import compression from "compression";
import { isProduction } from "../../../config";
import { bootstrapApp } from "../utils/bootstrapApp";
import { LoggerImpl } from "../logger";
import { Logger } from "../logger/ILogger";

const app = bootstrapApp();

const origin = {
  origin: isProduction ? process.env.API_URL : "*",
};

app.use(cors(origin));
app.use(compression());
app.use(helmet());
app.use(morgan("combined"));

const port = process.env.PORT || 5000;
const logger: Logger = LoggerImpl;

// ideally this will be passed to a node.http or node.https server
// checking port, address in use, etc
// handling graceful shutdowns etc
app.listen(port, () => {
  logger.info(`[App]: Listening on port ${port}`);
});
