import { LoggerImpl } from "@shared/infrastructure/logger";
import { Result } from "./Result";
import { UseCaseError } from "./UseCaseError";

export class UnexpectedError extends Result<UseCaseError> {
  public constructor(err: string | Error) {
    super(false, {
      message: `An unexpected error occurred.`,
      error: err,
    } as UseCaseError);
    const logger = LoggerImpl;
    logger.info(`[AppError]: An unexpected error occurred`);
    logger.error(err);
  }

  public static create(err: string | Error): UnexpectedError {
    return new UnexpectedError(err);
  }
}
