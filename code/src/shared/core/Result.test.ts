import { Result } from "./Result";

describe("Shared:Result", () => {
  it("creates successful result", () => {
    const result = Result.ok("foo");
    expect(result.isFailure).toBeFalsy();
    expect(result.isSuccess).toBeTruthy();
    expect(result.getValue()).toBe("foo");
  });

  it("creates failure result", () => {
    const result = Result.fail("foo");
    expect(result.isFailure).toBeTruthy();
    expect(result.isSuccess).toBeFalsy();
    expect(result.errorValue()).toBe("foo");
  });

  it("can combine results", () => {
    const combineErrorSuccess = Result.combine([
      Result.ok("foo"),
      Result.fail("bar"),
    ]);
    expect(combineErrorSuccess.isFailure).toBeTruthy();
    expect(combineErrorSuccess.isSuccess).toBeFalsy();
    expect(combineErrorSuccess.errorValue()).toEqual("bar");

    const combineErrorError = Result.combine([
      Result.fail("foo"),
      Result.fail("bar"),
    ]);
    expect(combineErrorError.isFailure).toBeTruthy();
    expect(combineErrorError.isSuccess).toBeFalsy();

    const combineSuccessSuccess = Result.combine([
      Result.ok("foo"),
      Result.ok("bar"),
    ]);
    expect(combineSuccessSuccess.isFailure).toBeFalsy();
    expect(combineSuccessSuccess.isSuccess).toBeTruthy();
  });
});
