import type { Request, Response } from "express";

export interface UseCase<Request, Response> {
  execute(request?: Request): Promise<Response> | Response;
}
