import { Guard } from "./Guard";

describe("Shared:Guard", () => {
  it("againstNullOrUndefinedBulk", () => {
    const guardResult = Guard.againstNullOrUndefinedBulk([
      { argument: { foo: "foo" }, argumentName: "foo" },
      { argument: { bar: "bar" }, argumentName: "bar" },
    ]);

    expect(guardResult.succeeded).toBeTruthy();
  });

  it("againstAtLeast", () => {
    const guardResult = Guard.againstAtLeast(2, "f");
    expect(guardResult.succeeded).toBeFalsy();
  });

  it("againstAtMost", () => {
    const guardResult = Guard.againstAtMost(5, "fooooooo");
    expect(guardResult.succeeded).toBeFalsy();
  });
});
