import * as Joi from "joi";
import { JSDOM } from "jsdom";
import DOMPurify from "dompurify";
const { window } = new JSDOM("<!DOCTYPE html>");
const domPurify = DOMPurify(window as any as Window);

export class TextUtils {
  public static sanitize(unsafeText: string): string {
    return domPurify.sanitize(unsafeText);
  }

  public static validateWebURL(url: string): boolean {
    return !Joi.string().uri().required().validate(url).error;
  }

  public static validateEmailAddress(email: string): boolean {
    return !Joi.string()
      .email()
      .lowercase()
      .required()
      .validate(email, { convert: true }).error;
  }

  public static createRandomNumericString(numberDigits: number): string {
    const chars = "0123456789";
    let value = "";

    for (let i = numberDigits; i > 0; --i) {
      value += chars[Math.round(Math.random() * (chars.length - 1))];
    }

    return value;
  }
}
