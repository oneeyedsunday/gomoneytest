export interface IPaginationOptions {
  offset: number;
  limit: number;
  skip: number;
}
export interface IFixturePaginationOptions extends IPaginationOptions {
  isCompleted?: boolean;
}

export class PaginationOptions implements IPaginationOptions {
  protected readonly _limit: number;
  protected readonly _offset: number;

  get limit(): number {
    return this._limit;
  }

  get offset(): number {
    return this._offset;
  }

  get skip(): number {
    return this.offset ? Math.max(this.offset - 1, 1) * this.limit : 0;
  }

  protected constructor(offset?: number, limit?: number) {
    this._limit = limit ? (isNaN(limit) ? 10 : limit) : 10;
    this._offset = offset ? (isNaN(offset) ? 0 : offset) : 0;
  }

  public static FromObject(object: {
    [key: string]: number | string;
  }): PaginationOptions {
    return new PaginationOptions(Number(object.offset), Number(object.limit));
  }
}
