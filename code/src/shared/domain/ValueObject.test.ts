import { ValueObject } from "@shared/domain/ValueObject";

type Props = {
  a?: string;
  b?: string;
};

class AValueObject extends ValueObject<Props> {}
class BValueObject extends ValueObject<Props> {}

describe("Shared:Domain:ValueObject", () => {
  it("compares equality robustly", () => {
    const props = { a: "foo", b: "bar" };
    const aValueObject = new AValueObject(props);

    expect(aValueObject.equals()).toBeFalsy();
    expect(aValueObject.equals()).toBeFalsy();
    expect(aValueObject.equals(new BValueObject({}))).toBeFalsy();
    expect(aValueObject.equals(new BValueObject(props))).toBeTruthy();
  });
});
