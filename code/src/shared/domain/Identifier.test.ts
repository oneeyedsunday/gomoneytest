import { Identifier } from "./Identifier";

describe("Shared:Domain:Identifier", () => {
  type Foo = {
    a?: string;
  };

  class FooIdentifier extends Identifier<Foo> {}
  it("exposes value", () => {
    const value = { a: "Rea Sremmund" };
    const a = new FooIdentifier(value);
    expect(a.toValue()).toEqual(value);
  });

  it("stringifies", () => {
    const value = { a: "Rea Sremmund" };
    const a = new FooIdentifier(value);
    expect(a.toString()).toEqual(JSON.stringify(value));
  });

  it("checks equality", () => {
    const value = { a: "Rea Sremmund" };
    const a = new FooIdentifier(value);
    const b = new FooIdentifier({});

    expect(a.equals(b)).toBeFalsy();
    expect(a.equals(new FooIdentifier(value))).toBeTruthy();
  });
});
