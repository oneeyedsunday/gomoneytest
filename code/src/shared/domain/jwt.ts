export interface JWTClaims {
  userId: string;
  email: string;
  username: string;
  isAdmin: boolean;
}

export type JWTToken = string;

export type SessionId = string;

export type RefreshToken = string;
