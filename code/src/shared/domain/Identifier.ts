import safeJsonStringify from "@bugsnag/safe-json-stringify";
import { ObjectID } from "mongodb";

export class Identifier<T> {
  constructor(private value: T) {
    this.value = value;
  }

  equals(id?: Identifier<T>): boolean {
    if (id === null || id === undefined) {
      return false;
    }
    if (!(id instanceof this.constructor)) {
      return false;
    }
    return id.toValue() === this.value;
  }

  toString(): string {
    if (this.value instanceof ObjectID)
      return (this.value as unknown as ObjectID).toHexString();
    const hasNativeToString =
      !!(this.value as unknown as any).toString &&
      (this.value as any).toString() != "[object Object]";
    return hasNativeToString
      ? (this.value as unknown as any).toString()
      : safeJsonStringify(this.value);
  }

  /**
   * Return raw value of identifier
   */

  toValue(): T {
    return this.value;
  }
}
