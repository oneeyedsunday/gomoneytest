import { ObjectID } from "mongodb";
import { Identifier } from "./Identifier";

export class UniqueEntityID extends Identifier<string | ObjectID> {
  constructor(id?: string | ObjectID) {
    super(id ? id : new ObjectID());
  }

  equals(id?: UniqueEntityID): boolean {
    if (
      id?.toValue() instanceof ObjectID &&
      this.toValue() instanceof ObjectID
    ) {
      return (this.toValue() as ObjectID).equals(id.toValue() as ObjectID);
    }

    if (
      !!id &&
      id?.toValue() instanceof ObjectID &&
      typeof this.toValue() === "string"
    ) {
      return (id?.toValue() as ObjectID).equals(this.toValue());
    }

    return super.equals(id);
  }
}
