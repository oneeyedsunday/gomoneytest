import { IPaginationOptions, PaginationOptions } from "./PaginationOptions";

export interface ISearchOptions extends IPaginationOptions {
  query: string;
}

export class SearchOptions extends PaginationOptions implements ISearchOptions {
  private readonly _query: string;

  get query(): string {
    return this._query;
  }

  private constructor(search?: string, offset?: number, limit?: number) {
    super(offset, limit);
    this._query = search ?? "";
  }

  public static FromObject(object: {
    [key: string]: number | string;
    search: string;
    query: string;
  }): PaginationOptions {
    return new SearchOptions(
      object.query ?? object.search,
      Number(object.offset),
      Number(object.limit)
    );
  }
}
