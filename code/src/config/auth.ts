const authConfig = Object.freeze({
  secret: String(process.env.JWT_SECRET),
  tokenExpiryTime: Number(process.env.JWT_TOKEN_EXPIRE_IN_SEC || 300), // seconds => 5 minutes
});

export { authConfig };
