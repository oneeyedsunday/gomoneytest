const redisConfig = Object.freeze({
  redisServerPort: Number(process.env.REDIS_PORT || 6379),
  redisServerURL: process.env.REDIS_URL,
  redisConnectionString: process.env.REDIS_CONN_STRING,
  redisPassword: process.env.REDIS_PASSWORD,
});

export { redisConfig };
