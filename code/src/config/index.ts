import { redisConfig } from "./redis";
import { authConfig } from "./auth";

const isProduction = process.env.IS_PRODUCTION?.toLowerCase() === "true";

export { isProduction, redisConfig, authConfig };
