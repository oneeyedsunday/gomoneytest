import { FixtureDTO } from "./fixtureDTO";

export interface GetFixturesResponseDTO {
  fixtures: FixtureDTO[];
}
