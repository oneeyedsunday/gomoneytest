import { PopulatedTeamFromFixture } from "../repos/models/Fixture";

export interface FixtureDTO {
  title: string;
  shortTitle?: string;
  kickOff: Date;
  link: string;
  fixtureId: string;
  teams: [PopulatedTeamFromFixture, PopulatedTeamFromFixture];
  homeTeam: PopulatedTeamFromFixture | null;
}
