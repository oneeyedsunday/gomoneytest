import { ObjectID } from "mongodb";
import { UniqueEntityID } from "../../../shared/domain/UniqueEntityID";
import { PopulatedTeamFromFixture } from "../repos/models/Fixture";
import { FixtureMap, PopulatedFixturePersistence } from "./Fixture";

describe("Fixtures:Mappers:Fixture", () => {
  const teams = [
    {
      _id: new ObjectID().toHexString(),
      abbreviation: "FOO",
      name: "fooBar",
    },
    {
      _id: new ObjectID().toHexString(),
      abbreviation: "BAR",
      name: "barFoo",
    },
  ] as [PopulatedTeamFromFixture, PopulatedTeamFromFixture];
  it("maps toDomain without homeTeam", () => {
    const persistedFixture: PopulatedFixturePersistence = {
      startDate: new Date(),
      link: "",
      homeTeam: null,
      teams,
    };

    const asDomain = FixtureMap.toDomain(persistedFixture);

    ["fixtureId", "teams", "teamModels", "link", "date"].forEach((prop) => {
      expect(asDomain).toHaveProperty(prop);
    });
    expect(asDomain.teamModels).toBeInstanceOf(Array);
    expect(asDomain.teamModels.length).toBe(2);
  });

  it("maps toDomain with homeTeam", () => {
    const persistedFixture: PopulatedFixturePersistence = {
      startDate: new Date(),
      link: "",
      homeTeam: teams[1],
      teams,
    };

    const asDomain = FixtureMap.toDomain(persistedFixture);

    ["fixtureId", "teams", "teamModels", "link", "date"].forEach((prop) => {
      expect(asDomain).toHaveProperty(prop);
    });

    expect(asDomain.teamModels).toBeInstanceOf(Array);
    expect(asDomain.teamModels.length).toBe(2);
    expect(asDomain.teams.homeTeam!.equals(new UniqueEntityID(teams[1]._id)));
  });

  it("maps to persistence", async () => {
    const persistedFixture: PopulatedFixturePersistence = {
      startDate: new Date(),
      link: "",
      homeTeam: teams[1],
      teams,
    };

    const asDomain = FixtureMap.toDomain(persistedFixture);
    const asPersistence = await FixtureMap.toPersistence(asDomain);
    expect(asPersistence._id).toBeTruthy();
    expect(asPersistence.homeTeam).toEqual(persistedFixture.homeTeam!._id);
    expect(asPersistence.link).toEqual(persistedFixture.link);
    expect(persistedFixture.teams).toEqual(teams);
    expect(asPersistence).toHaveProperty(
      "startDate",
      persistedFixture.startDate
    );
  });

  it("maps to DTO", () => {
    const persistedFixture: PopulatedFixturePersistence = {
      startDate: new Date(),
      link: "",
      homeTeam: teams[1],
      teams,
    };

    const asDomain = FixtureMap.toDomain(persistedFixture);
    const asDTO = FixtureMap.toDTO(asDomain);
    expect(asDTO.title).toEqual(`${teams[0].name} vs ${teams[1].name}`);
    expect(asDTO.shortTitle).toEqual(
      `${teams[0].abbreviation} vs ${teams[1].abbreviation}`
    );
    expect(asDTO.kickOff).toEqual(persistedFixture.startDate);
    expect(asDTO.link).toEqual(persistedFixture.link);
    expect(asDTO.fixtureId).toBeTruthy();
    expect(asDTO.teams).toEqual(teams);
    expect(asDTO.homeTeam).toEqual(teams[1]);
  });
});
