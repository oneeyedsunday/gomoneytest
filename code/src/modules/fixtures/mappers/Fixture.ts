import { Mapper } from "@shared/infrastructure/Mapper";
import { ObjectID } from "mongodb";
import { Fixture } from "../domain/fixture";
import { FixtureDTO } from "../dtos/fixtureDTO";
import {
  FixtureModel,
  PopulatedFixtureModel,
  PopulatedTeamFromFixture,
} from "../repos/models/Fixture";

type Projection = "_id" | "startDate" | "teams" | "homeTeam" | "link";

export type FixturePersistence = Pick<FixtureModel, Projection>;

export type PopulatedFixturePersistence = Pick<
  PopulatedFixtureModel,
  Projection
>;

export class FixtureMap implements Mapper<Fixture, FixtureDTO> {
  toDomain(raw: any): Fixture {
    return FixtureMap.toDomain(raw);
  }

  toDTO(from: Fixture): FixtureDTO {
    return FixtureMap.toDTO(from);
  }

  toDTOFromSerialized(raw: any): FixtureDTO {
    return FixtureMap.toDTOFromSerialized(raw);
  }

  public static toDTO(fixture: Fixture): FixtureDTO {
    return {
      link: fixture.link ? fixture.link?.url : "",
      kickOff: fixture.date.value,
      fixtureId: fixture.fixtureId.id.toString(),
      teams: fixture.teamModels,
      homeTeam: fixture.teamModels
        ? (fixture.teamModels.find(
            (tDTO) => tDTO._id === fixture.teams.homeTeam?.toString()
          ) as PopulatedTeamFromFixture)
        : null,
      title: fixture.teamModels.map((tDTO) => tDTO.name).join(" vs "),
      shortTitle: fixture.teamModels
        .map((tDTO) => tDTO.abbreviation)
        .join(" vs "),
    };
  }

  public static toDTOFromSerialized(fixture: any): FixtureDTO {
    return {
      link: fixture.props.link ? fixture.props.link?.props.url : "",
      kickOff: new Date(fixture.props.date.props.date),
      fixtureId: fixture._id.value,
      teams: fixture.populatedTeams,
      homeTeam: fixture.populatedTeams
        ? (fixture.populatedTeams.find(
            (tDTO: any) =>
              tDTO._id === fixture.props.teams.props?.homeTeam?.value
          ) as PopulatedTeamFromFixture)
        : null,
      title: fixture.populatedTeams.map((tDTO: any) => tDTO.name).join(" vs "),
      shortTitle: fixture.populatedTeams
        .map((tDTO: any) => tDTO.abbreviation)
        .join(" vs "),
    };
  }

  public static toDomain(raw: PopulatedFixturePersistence): Fixture {
    const fixtureOrError = Fixture.createFromPersistence(raw);

    return (
      fixtureOrError.isSuccess ? fixtureOrError.getValue() : null
    ) as Fixture;
  }

  public static async toPersistence(
    fixture: Fixture
  ): Promise<FixturePersistence> {
    return {
      _id: fixture.fixtureId.id.toString(),
      startDate: fixture.date.value,
      teams: fixture.teams.teams.map(
        (id) => new ObjectID(id.toString())
      ) as unknown as [string, string],
      homeTeam: fixture.teams.homeTeam
        ? fixture.teams.homeTeam.toString()
        : null,
      link: fixture.link ? fixture.link.url : null,
    };
  }
}
