import { Result } from "@shared/core/Result";
import { UseCaseError } from "@shared/core/UseCaseError";

export class FixtureLinkAlreadyExistsError extends Result<UseCaseError> {
  constructor(link: string) {
    super(false, {
      message: `The link ${link} provided already exists`,
    } as UseCaseError);
  }
}
export class FixtureAlreadyHasLinkError extends Result<UseCaseError> {
  constructor(id: string) {
    super(false, {
      message: `The fixture ${id} already has a link`,
    } as UseCaseError);
  }
}

export class FixtureNotFoundError extends Result<UseCaseError> {
  constructor(id: string) {
    super(false, {
      message: `Couldn't find a fixture by id {${id}}.`,
    } as UseCaseError);
  }
}

export class HomeTeamNotInTeamsError extends Result<UseCaseError> {
  constructor() {
    super(false, {
      message: "Home Team must be amongst teams provided",
    });
  }
}
