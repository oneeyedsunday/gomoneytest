import request from "supertest";
import * as express from "express";
import { bootstrapApp } from "@shared/infrastructure/utils/bootstrapApp";
import { repo } from "../repos";
import { FixtureModel } from "../repos/models/Fixture";

let app: express.Application;

beforeAll(() => {
  app = bootstrapApp();
});

const username = `teams-admin-${Date.now()}`;
const email = `${username}@example.com`;
const password = "password";
let token: string;
let fixtureId: string;
const teams: Array<{ name: string; teamId?: string }> = [
  { name: "Foo" },
  { name: "Bar" },
  { name: "Baz" },
];

describe("Fixture end to end tests", () => {
  beforeAll(() => {
    return request(app)
      .post("/api/auth/signup")
      .set("Accept", "application/json")
      .send({ email, username, password, role: "admin" })
      .then(() =>
        request(app)
          .post("/api/auth/login")
          .set("Accept", "application/json")
          .send({ username, password })
          .then((res) => {
            token = `Bearer ${res.body.data.accessToken}`;
          })
      )
      .then(() => {
        // create teams
        return request(app)
          .post("/api/teams")
          .set("Accept", "application/json")
          .set("Authorization", token)
          .send(teams[0])
          .then(() =>
            request(app)
              .post("/api/teams")
              .set("Accept", "application/json")
              .set("Authorization", token)
              .send(teams[1])
          )
          .then(() =>
            request(app)
              .post("/api/teams")
              .set("Accept", "application/json")
              .set("Authorization", token)
              .send(teams[2])
          );
      })
      .then(() =>
        request(app)
          .get("/api/teams?offset=0&limit=3")
          .set("Accept", "application/json")
          .set("Authorization", token)
          .then((res) => {
            const teamModels: Array<{ name: string; teamId: string }> =
              res.body.data.teams;
            teams.forEach((t) => {
              // find teamModel created from team
              t.teamId = teamModels.find((tM) => tM.name === t.name)!.teamId;
            });
          })
      );
  });

  it("creates fixture", () => {
    return request(app)
      .post("/api/fixtures")
      .set("Accept", "application/json")
      .set("Authorization", token)
      .send({
        date: new Date("2020-12-17T03:24:00"),
        teams: teams.slice(0, 2).map((t) => t.teamId),
      })
      .expect(201);
  });

  it("creates fixture with homeTeam", () => {
    return request(app)
      .post("/api/fixtures")
      .set("Accept", "application/json")
      .set("Authorization", token)
      .send({
        date: new Date("2023-01-17T03:24:00"),
        teams: teams.slice(0, 2).map((t) => t.teamId),
        homeTeam: teams[0].teamId,
      })
      .expect(201);
  });

  it("fails to create fixture if homeTeam provided is not amongst teams", () => {
    return request(app)
      .post("/api/fixtures")
      .set("Accept", "application/json")
      .set("Authorization", token)
      .send({
        date: new Date("2023-01-17T03:24:00"),
        teams: teams.slice(0, 2).map((t) => t.teamId),
        homeTeam: teams[2].teamId,
      })
      .expect(400)
      .then((res) => {
        expect(res.body.message).toBe(
          "Home Team must be amongst teams provided"
        );
      });
  });

  it("lists fixtures", () => {
    return request(app)
      .get("/api/fixtures")
      .set("Accept", "application/json")
      .set("Authorization", token)
      .expect(200)
      .then((res) => {
        expect(res.body.status).toEqual("success");
        expect(res.body.data).toBeTruthy();
        expect(res.body.data).toHaveProperty("fixtures");
        const { fixtures } = res.body.data;
        expect(fixtures.length).toBeTruthy();
        expect(fixtures[0]).toHaveProperty("fixtureId");
        fixtureId = fixtures[1].fixtureId;
        expect(fixtures[0]).toHaveProperty("link");
        expect(fixtures[0]).toHaveProperty("kickOff");
        expect(fixtures[0]).toHaveProperty("title");
        expect(fixtures[0]).toHaveProperty("shortTitle");
        expect(fixtures[0]).toHaveProperty("teams");
        fixtures.forEach((fixture: any) => {
          expect(fixture.teams.length).toBe(2);
          expect(fixture.teams[0]).toHaveProperty("_id");
          expect(fixture.teams[1]).toHaveProperty("_id");
          if (fixture.homeTeam) {
            expect(fixture.homeTeam).toHaveProperty("_id");
          }
        });
      });
  });

  it("handles pagination", () => {
    return request(app)
      .get("/api/fixtures?offset=0&limit=1")
      .set("Accept", "application/json")
      .set("Authorization", token)
      .expect(200)
      .then((res) => {
        expect(res.body.status).toEqual("success");
        expect(res.body.data).toBeTruthy();
        expect(res.body.data).toHaveProperty("fixtures");
        const { fixtures } = res.body.data;
        expect(fixtures.length).toBe(1);
      });
  });

  it("lists completed fixtures", () => {
    return request(app)
      .get("/api/fixtures?offset=0&limit=1&completed=true")
      .set("Accept", "application/json")
      .set("Authorization", token)
      .expect(200)
      .then((res) => {
        expect(res.body.status).toEqual("success");
        expect(res.body.data).toBeTruthy();
        expect(res.body.data).toHaveProperty("fixtures");
        const { fixtures } = res.body.data;
        expect(fixtures.length).toBe(1);
      });
  });

  it("lists pending fixtures", () => {
    return request(app)
      .get("/api/fixtures?offset=0&limit=1&completed=false")
      .set("Accept", "application/json")
      .set("Authorization", token)
      .expect(200)
      .then((res) => {
        expect(res.body.status).toEqual("success");
        expect(res.body.data).toBeTruthy();
        expect(res.body.data).toHaveProperty("fixtures");
        const { fixtures } = res.body.data;
        expect(fixtures.length).toBe(1);
      });
  });

  it("updates fixture", () => {
    const date = new Date("2023-01-17T03:24:00");
    return request(app)
      .put(`/api/fixtures/${fixtureId}`)
      .set("Accept", "application/json")
      .set("Authorization", token)
      .send({
        homeTeam: null,
        date,
        teams: teams.slice(0, 2).map((t) => t.teamId),
      })
      .expect(200)
      .then((res) => {
        expect(res.body.data.homeTeam).toBeFalsy();
        expect(new Date(res.body.data.kickOff).valueOf()).toEqual(
          date.valueOf()
        );
      });
  });

  it("generates link", () => {
    const nonce = `${Date.now()}-${Math.random() * 100}`;
    return request(app)
      .patch(`/api/fixtures/${fixtureId}/generate-link`)
      .set("Accept", "application/json")
      .set("Authorization", token)
      .send({
        link: `https://github.com/random-guys/backend-developer-test-${nonce}`,
      })
      .expect(201);
  });

  function wrapCallbackInPromise<T>(func: any, ...args: any[]): Promise<T> {
    return new Promise((resolve, reject) => {
      func(...args, (err: Error, result: T) => {
        if (err) return reject(err);
        resolve(result);
      });
    });
  }

  it("deletes fixture", () => {
    // Caching means we dont get correct count
    // just grab from repo
    return Promise.all([
      wrapCallbackInPromise<number>(repo.model.countDocuments.bind(repo.model)),
      wrapCallbackInPromise<FixtureModel>(
        repo.model.findOne.bind(repo.model),
        {}
      ),
    ]).then(([count, fixture]) =>
      request(app)
        .delete(`/api/fixtures/${fixture?._id}`)
        .set("Accept", "application/json")
        .set("Authorization", token)
        .expect(201)
        .then(() =>
          request(app)
            .get("/api/fixtures?offset=0&limit=5000")
            .set("Accept", "application/json")
            .set("Authorization", token)
            .then((res) => {
              expect(res.body.data.fixtures.length + 1).toEqual(count);
            })
        )
    );
  });
});
