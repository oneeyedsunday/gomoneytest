import request from "supertest";
import * as express from "express";
import { bootstrapApp } from "@shared/infrastructure/utils/bootstrapApp";

let app: express.Application;

beforeAll(() => {
  app = bootstrapApp();
});

const username = `teams-user-${Date.now()}`;
const email = `${username}@example.com`;
const password = "password";
let token: string;
let fixtureId: string;
const teams: Array<{ name: string; teamId?: string }> = [
  { name: "Foo" },
  { name: "Bar" },
  { name: "Baz" },
];

describe("Teams end to end tests", () => {
  beforeAll(() => {
    return request(app)
      .post("/api/auth/signup")
      .set("Accept", "application/json")
      .send({ email, username, password, role: "user" })
      .then(() =>
        request(app)
          .post("/api/auth/login")
          .set("Accept", "application/json")
          .send({ username, password })
          .then((res) => {
            token = `Bearer ${res.body.data.accessToken}`;
          })
      );
  });

  it("usr should not be able to creates fixture", () => {
    return request(app)
      .post("/api/fixtures")
      .set("Accept", "application/json")
      .set("Authorization", token)
      .send({
        date: new Date("2020-12-17T03:24:00"),
        teams: teams.slice(0, 2).map((t) => t.teamId),
      })
      .expect(403);
  });

  it("user can list fixtures", () => {
    return request(app)
      .get("/api/fixtures")
      .set("Accept", "application/json")
      .set("Authorization", token)
      .expect(200)
      .then((res) => {
        expect(res.body.status).toEqual("success");
        expect(res.body.data).toBeTruthy();
        expect(res.body.data).toHaveProperty("fixtures");
        const { fixtures } = res.body.data;
        expect(fixtures.length).toBeTruthy();
        expect(fixtures[0]).toHaveProperty("fixtureId");
        fixtureId = fixtures[1].fixtureId;
        expect(fixtures[0]).toHaveProperty("link");
        expect(fixtures[0]).toHaveProperty("kickOff");
        expect(fixtures[0]).toHaveProperty("title");
        expect(fixtures[0]).toHaveProperty("shortTitle");
        expect(fixtures[0]).toHaveProperty("teams");
        fixtures.forEach((fixture: any) => {
          expect(fixture.teams.length).toBe(2);
          expect(fixture.teams[0]).toHaveProperty("_id");
          expect(fixture.teams[1]).toHaveProperty("_id");
          if (fixture.homeTeam) {
            expect(fixture.homeTeam).toHaveProperty("_id");
          }
        });
      });
  });

  it("user should not be able to update fixture", () => {
    const date = new Date("2023-01-17T03:24:00");
    return request(app)
      .put(`/api/fixtures/${fixtureId}`)
      .set("Accept", "application/json")
      .set("Authorization", token)
      .send({
        homeTeam: null,
        date,
        teams: teams.slice(0, 2).map((t) => t.teamId),
      })
      .expect(403);
  });

  it("user should not be able to generates link", () => {
    return request(app)
      .patch(`/api/fixtures/${fixtureId}/generate-link`)
      .set("Accept", "application/json")
      .set("Authorization", token)
      .send({
        link: `https://github.com/random-guys/backend-developer-test-${Date.now()}`,
      })
      .expect(403);
  });

  it("user should not be able to delete fixture", () => {
    return request(app)
      .delete(`/api/fixtures/${fixtureId}`)
      .set("Accept", "application/json")
      .set("Authorization", token)
      .expect(403);
  });
});
