import { Fixture } from "../fixture";

export class LinkGeneratorService {
  private static getLinkPathFromFixture(fixture: Fixture): string {
    const [teamOne, teamTwo] = fixture.teamModels.map((t) =>
      t.abbreviation.toLowerCase()
    );
    return `${teamOne}-${teamTwo}-${Date.now()}`;
  }

  private static makeToUrl(value: string): URL {
    return new URL(value, process.env.LINK_GEN_BASE_URL);
  }

  public static generateLink(fixture: Fixture): string {
    const randomParts = LinkGeneratorService.getLinkPathFromFixture(fixture);
    return LinkGeneratorService.makeToUrl(randomParts).href;
  }
}
