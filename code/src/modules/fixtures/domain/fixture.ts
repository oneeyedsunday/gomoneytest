import { Guard } from "@shared/core/Guard";
import { Either, left, Result, right } from "@shared/core/Result";
import { AggregateRoot } from "@shared/domain/AggregateRoot";
import { UniqueEntityID } from "@shared/domain/UniqueEntityID";
import { PopulatedFixturePersistence } from "../mappers/Fixture";
import { PopulatedTeamFromFixture } from "../repos/models/Fixture";
import { FixtureDate } from "./fixtureDate";
import { FixtureId } from "./fixtureId";
import { FixtureLink } from "./fixtureLink";
import { FixtureTeams } from "./fixtureTeams";
import { FixtureCreated } from "./events/fixtureCreated";
import { FixtureDeleted } from "./events/fixtureDeleted";
import * as FixtureErrors from "@modules/fixtures/core/FixtureErrors";
import { FixtureLinkGenerated } from "./events/fixtureLinkGenerated";
import { FixtureUpdated } from "./events/fixtureUpdated";

type Teams = [PopulatedTeamFromFixture, PopulatedTeamFromFixture];

type FixtureUpdateResult = Either<
  FixtureErrors.FixtureAlreadyHasLinkError,
  Result<void>
>;
interface FixtureProps {
  date: FixtureDate;
  teams: FixtureTeams;
  link: FixtureLink;
}

export class Fixture extends AggregateRoot<FixtureProps> {
  private populatedTeams?: Teams;
  public static readonly fixtureDurationMinutes = 120;
  get fixtureId(): FixtureId {
    return FixtureId.create(this._id).getValue();
  }

  get teams(): FixtureTeams {
    return this.props.teams;
  }

  get teamModels(): Teams {
    return this.populatedTeams as Teams;
  }

  get link(): FixtureLink | null {
    return this.props.link;
  }

  get date(): FixtureDate {
    return this.props.date;
  }

  private get isPublicReady(): boolean {
    return !!this.link?.url;
  }

  private constructor(
    props: FixtureProps,
    id?: UniqueEntityID,
    populatedTeams?: Teams
  ) {
    super(props, id);
    this.populatedTeams = populatedTeams;
  }

  public updateTeams(fixtureTeams: FixtureTeams): void {
    // if restrictions
    // run logic here
    // and return Either<Failure, Success>
    if (!this.props.teams.equals(fixtureTeams))
      this.addDomainEvent(new FixtureUpdated(this));
    this.props.teams = fixtureTeams;
  }

  public updateDate(fixtureDate: FixtureDate): void {
    if (!this.props.date.equals(fixtureDate))
      this.addDomainEvent(new FixtureUpdated(this));
    this.props.date = fixtureDate;
  }

  public updateLink(fixtureLink: FixtureLink): FixtureUpdateResult {
    if (this.link && this.link.url) {
      return left(
        new FixtureErrors.FixtureAlreadyHasLinkError(this._id.toString())
      );
    }

    this.addDomainEvent(new FixtureLinkGenerated(this));
    this.props.link = fixtureLink;
    return right(Result.ok<void>());
  }

  public delete(): void {
    this.addDomainEvent(new FixtureDeleted(this));
  }

  // Relax validation
  // Derive another class PopulatedFixture that extends this
  // That class should have this method
  // or just set teamModels to null
  // This is me basically making an ORM constrained by types ;)
  public static createFromPersistence(
    fixture: PopulatedFixturePersistence
  ): Result<Fixture> {
    const populatedTeams = fixture.teams;
    const link = FixtureLink.create(
      { url: fixture.link },
      !fixture._id || !fixture.link
    ).getValue();
    const date = FixtureDate.create({ date: fixture.startDate }).getValue();
    return Result.ok<Fixture>(
      new Fixture(
        {
          date,
          link,
          teams: FixtureTeams.create({
            teams: [
              new UniqueEntityID(fixture.teams[0]?._id),
              new UniqueEntityID(fixture.teams[1]?._id),
            ],
            homeTeam: fixture.homeTeam
              ? new UniqueEntityID(fixture.homeTeam?._id)
              : null,
          }).getValue(),
        },
        new UniqueEntityID(fixture._id),
        populatedTeams
      )
    );
  }

  public static create(
    props: FixtureProps,
    id?: UniqueEntityID
  ): Result<Fixture> {
    const guardResult = Guard.isValidDate(props.date?.props?.date);
    if (!guardResult.succeeded)
      return Result.fail<Fixture>(guardResult.message as string);

    const teamsOrError = FixtureTeams.create(props.teams.props);

    if (!teamsOrError.isSuccess)
      return Result.fail<Fixture>(teamsOrError.error as string);

    const isNewFixture = !!id == false;

    const link =
      isNewFixture && !props.link
        ? (null as unknown as FixtureLink)
        : props.link;

    if (!isNewFixture) {
      const linkOrError = FixtureLink.create({ url: props.link.url });
      if (!linkOrError.isSuccess)
        Result.fail<Fixture>(linkOrError.error as string);
    }

    const fixture = new Fixture({ ...props, link }, id);

    if (isNewFixture) fixture.addDomainEvent(new FixtureCreated(fixture));

    if (isNewFixture && fixture.isPublicReady)
      fixture.addDomainEvent(new FixtureLinkGenerated(fixture));
    return Result.ok<Fixture>(fixture);
  }
}
