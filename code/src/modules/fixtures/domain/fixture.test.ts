import { UniqueEntityID } from "../../../shared/domain/UniqueEntityID";
import { Fixture } from "./fixture";
import { FixtureDate } from "./fixtureDate";
import { FixtureLink } from "./fixtureLink";
import { FixtureTeams } from "./fixtureTeams";

describe("Fixtures:Domain:Fixture", () => {
  it("validates", () => {
    expect(
      Fixture.create({
        link: FixtureLink.create({ url: "https://google.com" }).getValue(),
        date: FixtureDate.create({ date: new Date() }).getValue(),
        teams: {} as unknown as FixtureTeams,
      }).isSuccess
    ).toBeFalsy();
  });

  it("creates", () => {
    expect(
      Fixture.create({
        link: FixtureLink.create({ url: "https://google.com" }).getValue(),
        date: FixtureDate.create({ date: new Date() }).getValue(),
        teams: FixtureTeams.create({
          teams: [new UniqueEntityID(), new UniqueEntityID()],
        }).getValue(),
      }).isSuccess
    ).toBeTruthy();
  });

  it("emits creation event at creation", () => {
    const fixture = Fixture.create({
      link: FixtureLink.create({ url: "https://google.com" }).getValue(),
      date: FixtureDate.create({ date: new Date() }).getValue(),
      teams: FixtureTeams.create({
        teams: [new UniqueEntityID(), new UniqueEntityID()],
      }).getValue(),
    });

    expect(fixture.getValue().domainEvents.length).toEqual(1);
  });

  it("yields internals", () => {
    const fixture = Fixture.create({
      link: FixtureLink.create({ url: "https://google.com" }).getValue(),
      date: FixtureDate.create({ date: new Date() }).getValue(),
      teams: FixtureTeams.create({
        teams: [new UniqueEntityID(), new UniqueEntityID()],
      }).getValue(),
    }).getValue();

    ["fixtureId", "id", "teams", "link", "date"].forEach((prop) => {
      expect(fixture).toHaveProperty(prop);
    });
  });
});
