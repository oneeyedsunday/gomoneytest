import { UniqueEntityID } from "@shared/domain/UniqueEntityID";
import { FixtureId } from "./fixtureId";

describe("Fixtures:Domain:FixtureId", () => {
  it("create FixtureId and fills even if no id provided", () => {
    const teamId = FixtureId.create();
    expect(teamId.isSuccess).toBeTruthy();
    expect(!!teamId.getValue().id).toBeTruthy();
  });

  it("create FixtureId from id", () => {
    const id = new UniqueEntityID("foo");
    const fixtureId = FixtureId.create(id);
    expect(fixtureId.isSuccess).toBeTruthy();
    expect(!!fixtureId.getValue().id).toBeTruthy();
    expect(fixtureId.getValue().id.toString()).toEqual("foo");
  });
});
