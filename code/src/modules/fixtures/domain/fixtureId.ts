import { Result } from "@shared/core/Result";
import { Entity } from "@shared/domain/Entity";
import { UniqueEntityID } from "@shared/domain/UniqueEntityID";

export class FixtureId extends Entity<UniqueEntityID | null> {
  get id(): UniqueEntityID {
    return this._id;
  }

  private constructor(id?: UniqueEntityID) {
    super(null, id);
  }

  public static create(id?: UniqueEntityID): Result<FixtureId> {
    return Result.ok<FixtureId>(new FixtureId(id));
  }
}
