import { Result } from "@shared/core/Result";
import { UniqueEntityID } from "@shared/domain/UniqueEntityID";
import { ValueObject } from "@shared/domain/ValueObject";
import * as FixtureErrors from "@modules/fixtures/core/FixtureErrors";

type TeamIdentifier = UniqueEntityID;

export interface FixtureTeamsProps {
  teams: [TeamIdentifier, TeamIdentifier];
  homeTeam?: TeamIdentifier | null;
}

export class FixtureTeams extends ValueObject<FixtureTeamsProps> {
  get teams(): TeamIdentifier[] {
    return this.props.teams;
  }

  get homeTeam(): TeamIdentifier | null {
    return this.props.homeTeam ? this.props.homeTeam : null;
  }

  private constructor(props: FixtureTeamsProps) {
    super(props);
  }

  public static create(teamsConfig: FixtureTeamsProps): Result<FixtureTeams> {
    if (!((teamsConfig?.teams || []).filter((t) => !!t).length === 2))
      return Result.fail<FixtureTeams>("Two teams required");

    if (teamsConfig.teams[0].equals(teamsConfig.teams[1]))
      return Result.fail<FixtureTeams>("Teams must be different");

    if (
      !!teamsConfig.homeTeam &&
      !(teamsConfig.teams || []).find(
        (team) => team.toValue() === teamsConfig.homeTeam?.toValue()
      )
    )
      return new FixtureErrors.HomeTeamNotInTeamsError() as unknown as Result<FixtureTeams>;

    return Result.ok<FixtureTeams>(new FixtureTeams(teamsConfig));
  }
}
