import { FixtureLink } from "./fixtureLink";

describe("Fixtures:Domain:FixtureLink", () => {
  it("guards againt null ", () => {
    expect(
      FixtureLink.create({ url: null as unknown as string }).isFailure
    ).toBeTruthy();
  });

  it("validates against invalid url", () => {
    expect(FixtureLink.create({ url: "foo" }).isFailure).toBeTruthy();
  });

  it("allows valid url", () => {
    const url =
      "https://www.livescores.com/soccer/england/premier-league/everton-vs-tottenham-hotspur/267677/";
    expect(FixtureLink.create({ url }).isSuccess).toBeTruthy();
  });
});
