import { Fixture } from "../../domain/fixture";
import { FixtureMap } from "../../mappers/Fixture";
import { ObjectID } from "mongodb";
import { FixtureCreated } from "./fixtureCreated";

describe("Fixtures:Domain:Events:FixtureCreated", () => {
  it("sanity checks for fixture created event", async () => {
    const _id = new ObjectID();
    const fixture: Fixture = FixtureMap.toDomain({
      _id: _id.toHexString(),
      startDate: new Date(),
      link: "",
      homeTeam: null,
      teams: [
        {
          _id: new ObjectID().toHexString(),
          abbreviation: "FOO",
          name: "fooBar",
        },
        {
          _id: new ObjectID().toHexString(),
          abbreviation: "BAR",
          name: "barFoo",
        },
      ],
    });
    const now = Date.now();
    const event = new FixtureCreated(fixture);
    expect(event.fixture).toEqual(fixture);
    expect(event.dateTimeOccurred.valueOf()).toBeGreaterThanOrEqual(now);
    expect(event.getAggregateId().toValue()).toEqual(_id.toHexString());
  });
});
