import { IDomainEvent } from "@shared/domain/events/IDomainEvent";
import { UniqueEntityID } from "@shared/domain/UniqueEntityID";
import { Fixture } from "../fixture";

export class FixtureLinkGenerated implements IDomainEvent {
  public dateTimeOccurred: Date;
  public fixture: Fixture;

  constructor(fixture: Fixture) {
    this.dateTimeOccurred = new Date();
    this.fixture = fixture;
  }

  getAggregateId(): UniqueEntityID {
    return this.fixture.id;
  }
}
