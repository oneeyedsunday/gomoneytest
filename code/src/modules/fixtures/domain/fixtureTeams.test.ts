import { UniqueEntityID } from "@shared/domain/UniqueEntityID";
import { FixtureTeams, FixtureTeamsProps } from "./fixtureTeams";

describe("Fixtures:Domain:FixtureTeams", () => {
  let teamOne: UniqueEntityID;
  let teamTwo: UniqueEntityID;
  beforeAll(() => {
    teamOne = new UniqueEntityID();
    teamTwo = new UniqueEntityID();
  });

  it("validates against no teams", () => {
    expect(FixtureTeams.create({} as FixtureTeamsProps).isFailure).toBeTruthy();
  });
  it("validates against no first team", () => {
    expect(
      FixtureTeams.create({
        teams: [null, teamTwo],
      } as unknown as FixtureTeamsProps).isFailure
    ).toBeTruthy();
  });
  it("validates against no second team", () => {
    expect(
      FixtureTeams.create({
        teams: [teamOne, null],
      } as unknown as FixtureTeamsProps).isFailure
    ).toBeTruthy();
  });
  it("validates that home team is participating", () => {
    expect(
      FixtureTeams.create({
        teams: [teamOne, teamTwo],
        homeTeam: new UniqueEntityID(),
      }).isFailure
    ).toBeTruthy();
  });
  it("validates that teams are distinct", () => {
    expect(
      FixtureTeams.create({
        teams: [teamOne, teamOne],
      }).isFailure
    ).toBeTruthy();
  });
  it("creates", () => {
    expect(
      FixtureTeams.create({
        teams: [teamOne, teamTwo],
      }).isSuccess
    ).toBeTruthy();
  });
  it("creates with home team", () => {
    const teams = FixtureTeams.create({
      teams: [teamOne, teamTwo],
      homeTeam: teamTwo,
    });
    expect(teams.isSuccess).toBeTruthy();
    expect(teams.getValue().homeTeam?.equals(teamTwo)).toBeTruthy();
  });
});
