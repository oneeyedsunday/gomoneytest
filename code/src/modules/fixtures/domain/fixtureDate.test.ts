import { FixtureDate } from "./fixtureDate";

describe("Fixtures:Domain:FixtureDate", () => {
  it("validates against invalid dates - string", () => {
    expect(FixtureDate.create({ date: "ssss" }).isFailure).toBeTruthy();
  });

  it("validates against invalid date - Date Object", () => {
    expect(
      FixtureDate.create({ date: new Date(new Date("sssss")) }).isFailure
    ).toBeTruthy();
  });

  it("allows valid date from string", () => {
    expect(
      FixtureDate.create({ date: "December 17, 1995 03:24:00" }).isSuccess
    ).toBeTruthy();
    expect(
      FixtureDate.create({ date: "1995-12-17T03:24:00" }).isSuccess
    ).toBeTruthy();
  });

  it("allows valid date from Date Object", () => {
    expect(
      FixtureDate.create({ date: new Date(new Date("sssss")) }).isFailure
    ).toBeTruthy();
  });
});
