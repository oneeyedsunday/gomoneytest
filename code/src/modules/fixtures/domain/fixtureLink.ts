import { Guard } from "@shared/core/Guard";
import { Result } from "@shared/core/Result";
import { ValueObject } from "@shared/domain/ValueObject";
import { TextUtils } from "@shared/utils/TextUtils";

interface FixtureLinkProps {
  url: string;
}

export class FixtureLink extends ValueObject<FixtureLinkProps> {
  get url(): string {
    return this.props.url;
  }

  private constructor(props: FixtureLinkProps) {
    super(props);
  }

  public static create(
    props: FixtureLinkProps,
    ignoreEmpty?: boolean
  ): Result<FixtureLink> {
    if (!ignoreEmpty) {
      const nullGuard = Guard.againstNullOrUndefined(props.url, "url");

      if (!nullGuard.succeeded) {
        return Result.fail<FixtureLink>(nullGuard.message as string);
      }
    }

    if (props.url && !TextUtils.validateWebURL(props.url)) {
      return Result.fail<FixtureLink>(`Url {${props.url}} is not valid.`);
    }

    return Result.ok<FixtureLink>(new FixtureLink(props));
  }
}
