import { Guard } from "@shared/core/Guard";
import { Result } from "@shared/core/Result";
import { ValueObject } from "@shared/domain/ValueObject";

interface FixtureDateProps {
  date: Date | string;
}

export class FixtureDate extends ValueObject<FixtureDateProps> {
  get value(): Date {
    return new Date(this.props.date);
  }

  private constructor(props: FixtureDateProps) {
    super(props);
  }

  public static create(props: FixtureDateProps): Result<FixtureDate> {
    const dateResult = Guard.combine([
      Guard.againstNullOrUndefined(props.date, "fixtureDate"),
      Guard.isValidDate(props.date),
    ]);
    if (!dateResult.succeeded)
      return Result.fail<FixtureDate>(dateResult.message as string);

    return Result.ok<FixtureDate>(new FixtureDate(props));
  }
}
