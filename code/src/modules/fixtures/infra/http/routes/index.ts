import { createFixtureController } from "@modules/fixtures/useCases/createFixture";
import { deleteFixtureController } from "@modules/fixtures/useCases/deleteFixture";
import { generateFixtureLinkController } from "@modules/fixtures/useCases/generateFixtureLink";
import { getAllFixturesController } from "@modules/fixtures/useCases/getAllFixtures";
import { getFixturesByCompletionStateController } from "@modules/fixtures/useCases/getFixturesByCompletionState";
import { updateFixtureController } from "@modules/fixtures/useCases/updateFixture";
import { adminMiddleware } from "@shared/infrastructure/http";
import express from "express";
const fixtureRouter = express.Router();

fixtureRouter.get("", (req, res) => {
  const hasCompletionStateFilter = Object.keys(req.query).includes("completed");
  switch (true) {
    case hasCompletionStateFilter:
      return getFixturesByCompletionStateController.execute(req, res);
    default:
      return getAllFixturesController.execute(req, res);
  }
});

fixtureRouter.post("", adminMiddleware.ensureIsAdmin(), (req, res) =>
  createFixtureController.execute(req, res)
);
fixtureRouter.put("/:id", adminMiddleware.ensureIsAdmin(), (req, res) =>
  updateFixtureController.execute(req, res)
);
fixtureRouter.patch(
  "/:id/generate-link",
  adminMiddleware.ensureIsAdmin(),
  (req, res) => generateFixtureLinkController.execute(req, res)
);
fixtureRouter.delete("/:id", adminMiddleware.ensureIsAdmin(), (req, res) =>
  deleteFixtureController.execute(req, res)
);

export { fixtureRouter };
