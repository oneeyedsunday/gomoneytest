import { Fixture } from "@modules/fixtures/domain/fixture";
import { IFixturePaginationOptions } from "@shared/domain/PaginationOptions";
import safeJsonStringify from "@bugsnag/safe-json-stringify";
import { RedisWriteAsideCache } from "@shared/infrastructure/cache/RedisCache";

type GetFixturesCacheOptions = IFixturePaginationOptions;

export class GetFixturesRedisCache extends RedisWriteAsideCache<
  Fixture[],
  GetFixturesCacheOptions
> {
  public async getData(
    options: GetFixturesCacheOptions
  ): Promise<Fixture[] | null> {
    try {
      const items = await this.getAsync(this.keyFactory(options));
      return JSON.parse(items as string) as Fixture[];
    } catch (err) {
      return null;
    }
  }
  public async setData(
    value: Fixture[],
    options: GetFixturesCacheOptions
  ): Promise<void> {
    await this.setAsync(this.keyFactory(options), safeJsonStringify(value));
  }
}
