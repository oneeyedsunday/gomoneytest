import { IFixturePaginationOptions } from "@shared/domain/PaginationOptions";
import { redisConnection } from "@shared/infrastructure/services/redis/connection";
import { GetFixturesRedisCache } from "./Fixture";

const ttl = Number(process.env.CACHE_TTL_SECONDS || 30);

const getFixturesWriteAsideCache = new GetFixturesRedisCache(
  redisConnection,
  ttl,
  (queryOpts: IFixturePaginationOptions) => {
    const isCompletedTag = Object.keys(queryOpts).includes("isCompleted")
      ? `-${queryOpts.isCompleted === false ? "false" : "true"}`
      : "";
    return `fixtures-${queryOpts.offset}-${queryOpts.limit}-${isCompletedTag}`;
  }
);

export { getFixturesWriteAsideCache };
