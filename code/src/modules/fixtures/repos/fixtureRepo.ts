import { IFixturePaginationOptions } from "@shared/domain/PaginationOptions";
import { Fixture } from "../domain/fixture";
import { FixtureId } from "../domain/fixtureId";

export interface IFixtureRepo {
  getFixtureById(
    fixtureId: FixtureId,
    ignoreNotFound?: boolean
  ): Promise<Fixture | null>;
  getFixtures(paginationOptions: IFixturePaginationOptions): Promise<Fixture[]>;
  exists(fixtureId: FixtureId): Promise<boolean>;
  save(fixture: Fixture): Promise<void>;
  delete(fixtureId: FixtureId): Promise<void>;
}
