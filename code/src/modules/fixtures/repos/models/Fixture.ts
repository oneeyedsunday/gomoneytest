import { Model } from "@random-guys/bucket";

// eslint-disable-next-line @typescript-eslint/no-unused-vars
interface BaseFixtureModel {
  startDate: Date;
  link: string;
}

// TODO (oneeyedsunday) use ts-mixer

export interface FixtureModel extends Model {
  startDate: Date;
  link: string | null;
  teams: [string, string];
  homeTeam: string | null;
}

export interface PopulatedTeamFromFixture {
  _id: string;
  abbreviation: string;
  name: string;
}

export interface PopulatedFixtureModel extends Model {
  startDate: Date;
  link: string;
  teams: [PopulatedTeamFromFixture, PopulatedTeamFromFixture];
  homeTeam: PopulatedTeamFromFixture | null;
}
