import { SchemaTypes, Schema } from "mongoose";
import { SchemaFactory } from "@random-guys/bucket";

export const FixtureSchema = new Schema(
  SchemaFactory({
    startDate: {
      type: SchemaTypes.Date,
      required: true,
    },
    link: {
      type: SchemaTypes.String,
      trim: true,
      index: {
        unique: true,
        partialFilterExpression: { link: { $type: "string" } },
      },
    },
    teams: {
      type: [
        {
          type: Schema.Types.ObjectId,
          ref: "Team",
        },
      ],
      required: true,
      validate: [
        (teams: Array<any>) => teams?.length === 2,
        "{PATH} must contain two items",
      ],
    },
    homeTeam: {
      type: Schema.Types.ObjectId,
      ref: "Team",
    },
  })
);
