import { MongoFixtureRepo } from "./implementations/mongo";
import { connection } from "@shared/infrastructure/database/mongo";
import { runHook } from "@shared/infrastructure/database/mongo/hook";
import { FixtureSchema } from "./schemas/Fixture";
import { FixturePersistence } from "../mappers/Fixture";
import { FixtureModel } from "./models/Fixture";

type Result = Promise<FixtureModel | null>;

// important to call this before compiling model (in our case in the repo)
// see https://mongoosejs.com/docs/middleware.html#defining
runHook<FixturePersistence, Result>(FixtureSchema, {
  save: (fixtureDoc) => populateFixture((fixtureDoc as any)._id),
});

function populateFixture(fixtureId: string): Result {
  return repo.model
    .findById(fixtureId)
    .populate({ path: "teams", select: MongoFixtureRepo.TeamProjection })
    .populate({
      path: "homeTeam",
      select: MongoFixtureRepo.TeamProjection,
    })
    .then();
}

const repo = new MongoFixtureRepo(connection);

export { repo };
