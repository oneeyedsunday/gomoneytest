import { BaseRepository } from "@random-guys/bucket";
import { FixtureModel, PopulatedFixtureModel } from "../models/Fixture";
import { FixtureSchema } from "../schemas/Fixture";
import * as mongoose from "mongoose";
import { FixtureId } from "@modules/fixtures/domain/fixtureId";
import { Fixture } from "@modules/fixtures/domain/fixture";
import { FixtureMap } from "@modules/fixtures/mappers/Fixture";
import { IFixturePaginationOptions } from "@shared/domain/PaginationOptions";
import { IFixtureRepo } from "../fixtureRepo";

export class FixtureNotFoundError extends Error {
  message = "Fixture not found.";
  name = "FixtureNotFound";
}

export class MongoFixtureRepo
  extends BaseRepository<FixtureModel>
  implements IFixtureRepo
{
  constructor(connection: mongoose.Connection) {
    super(connection, "Fixture", FixtureSchema);
  }
  public static readonly TeamProjection = ["name", "abbreviation", "_id"];
  async getFixtureById(
    fixtureId: FixtureId,
    ignoreNotFound?: boolean
  ): Promise<Fixture | null> {
    const populatedFixture = (await this.model
      .findById(fixtureId.id.toString())
      .populate({ path: "teams", select: MongoFixtureRepo.TeamProjection })
      .populate({
        path: "homeTeam",
        select: MongoFixtureRepo.TeamProjection,
      })) as unknown as PopulatedFixtureModel | null;

    if (!populatedFixture && !ignoreNotFound) throw new FixtureNotFoundError();

    if (populatedFixture) return FixtureMap.toDomain(populatedFixture);

    return null;
  }
  private buildFixturesQuery(fixtureQueryOpts: {
    isCompleted?: boolean;
  }): mongoose.FilterQuery<FixtureModel> {
    const fixtureEndDate = new Date();
    fixtureEndDate.setMinutes(
      fixtureEndDate.getMinutes() - Fixture.fixtureDurationMinutes
    );
    const isCompleteQuery = { $lte: fixtureEndDate };
    return Object.keys(fixtureQueryOpts).includes("isCompleted")
      ? {
          startDate: fixtureQueryOpts.isCompleted
            ? isCompleteQuery
            : { $not: isCompleteQuery },
        }
      : {};
  }

  getFixtures(
    paginationOptions: IFixturePaginationOptions
  ): Promise<Fixture[]> {
    return this.model
      .find(this.buildFixturesQuery(paginationOptions))
      .limit(paginationOptions.limit)
      .skip(paginationOptions.skip)
      .sort({ created_at: -1 })
      .populate({ path: "teams", select: MongoFixtureRepo.TeamProjection })
      .populate({ path: "homeTeam", select: MongoFixtureRepo.TeamProjection })
      .then((fixtureModels) =>
        fixtureModels.map((fM) =>
          FixtureMap.toDomain(fM as any as PopulatedFixtureModel)
        )
      );
  }
  async exists(fixtureId: FixtureId): Promise<boolean> {
    const baseFixture = await this.model.findById(fixtureId.id);
    return baseFixture != null;
  }
  async save(fixture: Fixture): Promise<void> {
    const isExistingFixture = await this.exists(fixture.fixtureId);

    if (!isExistingFixture) {
      const p = await FixtureMap.toPersistence(fixture);
      await this.create(p);
    } else {
      await this.update(fixture.id.toValue().toString(), {
        link: fixture.link ? fixture.link.url : "",
        teams: fixture.teams.teams.map((id) => id.toString()) as [
          string,
          string
        ],
        homeTeam: fixture.teams.homeTeam
          ? fixture.teams.homeTeam?.toString()
          : null,
        startDate: fixture.date.value,
      });
    }
  }
  async delete(fixtureId: FixtureId): Promise<void> {
    await new Promise<void>((resolve, reject) => {
      this.model.findOneAndDelete(
        { _id: fixtureId.id.toString() },
        null,
        (err) => {
          if (err) return reject(err);
          resolve();
        }
      );
    });
  }
}
