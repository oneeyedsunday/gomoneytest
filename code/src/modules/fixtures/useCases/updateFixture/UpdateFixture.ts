import { IFixtureRepo } from "@modules/fixtures/repos/fixtureRepo";
import { Result, left, right, Either } from "@shared/core/Result";
import { UseCase } from "@shared/core/UseCase";
import { UpdateFixtureDTO } from "./UpdateFixtureDTO";
import * as AppError from "@shared/core/AppError";
import * as FixtureErrors from "@modules/fixtures/core/FixtureErrors";
import { UniqueEntityID } from "@shared/domain/UniqueEntityID";
import { Fixture } from "@modules/fixtures/domain/fixture";
import { FixtureId } from "@modules/fixtures/domain/fixtureId";
import { FixtureDate } from "@modules/fixtures/domain/fixtureDate";
import { FixtureTeams } from "@modules/fixtures/domain/fixtureTeams";

type Response = Either<
  FixtureErrors.FixtureNotFoundError | AppError.UnexpectedError | Result<any>,
  Result<Fixture>
>;

export class UpdateFixture
  implements UseCase<UpdateFixtureDTO, Promise<Response>>
{
  private repo: IFixtureRepo;

  constructor(repo: IFixtureRepo) {
    this.repo = repo;
  }

  public async execute(request: UpdateFixtureDTO): Promise<Response> {
    let fixture: Fixture;

    const fixtureIdOrError = FixtureId.create(new UniqueEntityID(request.id));

    if (!fixtureIdOrError.isSuccess) return left(fixtureIdOrError);
    try {
      fixture = (await this.repo.getFixtureById(
        fixtureIdOrError.getValue()
      )) as Fixture;
    } catch (err) {
      return left(new FixtureErrors.FixtureNotFoundError(request.id));
    }

    const teamsOrError = FixtureTeams.create({
      teams: request.teams?.map((id) => new UniqueEntityID(id)) as any,
      ...(request.homeTeam
        ? { homeTeam: new UniqueEntityID(request.homeTeam) }
        : {}),
    });

    if (teamsOrError.isFailure) {
      return left(teamsOrError);
    }

    const dateOrError = FixtureDate.create({ date: request.startDate });

    if (dateOrError.isFailure) {
      return left(dateOrError);
    }

    fixture.updateDate(dateOrError.getValue());

    fixture.updateTeams(teamsOrError.getValue());

    await this.repo.save(fixture);

    return right(Result.ok<Fixture>(fixture));
  }
}
