import { repo } from "../../repos";
import { UpdateFixture } from "./UpdateFixture";
import { UpdateFixtureController } from "./UpdateFixtureController";

const updateFixture = new UpdateFixture(repo);
const updateFixtureController = new UpdateFixtureController(updateFixture);

export { updateFixture, updateFixtureController };
