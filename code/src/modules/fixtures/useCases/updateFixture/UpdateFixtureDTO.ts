export interface UpdateFixtureDTO {
  homeTeam?: string;
  startDate: string;
  teams: [string, string];
  id: string;
}
