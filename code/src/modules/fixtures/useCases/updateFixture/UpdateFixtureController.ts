import { Fixture } from "@modules/fixtures/domain/fixture";
import { FixtureMap } from "@modules/fixtures/mappers/Fixture";
import { Either } from "@shared/core/Result";
import { BaseController } from "@shared/infrastructure/http/controllers/Base";
import express from "express";
import { UpdateFixture } from "./UpdateFixture";
import { UpdateFixtureDTO } from "./UpdateFixtureDTO";
import * as FixtureErrors from "@modules/fixtures/core/FixtureErrors";

export class UpdateFixtureController extends BaseController {
  private useCase: UpdateFixture;

  constructor(useCase: UpdateFixture) {
    super();
    this.useCase = useCase;
  }

  async executeImpl(req: express.Request, res: express.Response): Promise<any> {
    const dto: UpdateFixtureDTO = {
      teams: req.body.teams,
      homeTeam: req.body.homeTeam,
      startDate: req.body.date,
      id: req.params.id,
    };

    try {
      const result = await this.useCase.execute(dto);
      if (result.isLeft()) {
        const error = result.value;
        switch (error.constructor) {
          case FixtureErrors.FixtureNotFoundError:
            return this.notFound(res, error.errorValue().message);
          default:
            return this.fail(res, error.errorValue().message);
        }
      } else {
        const team = (result as Either<any, Fixture>).value.getValue();
        return this.ok(res, FixtureMap.toDTO(team));
      }
    } catch (err) {
      return this.fail(res, err);
    }
  }
}
