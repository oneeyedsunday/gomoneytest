import { IFixtureRepo } from "@modules/fixtures/repos/fixtureRepo";
import { Result, left, right, Either } from "@shared/core/Result";
import { UseCase } from "@shared/core/UseCase";
import * as AppError from "@shared/core/AppError";
import * as FixtureErrors from "@modules/fixtures/core/FixtureErrors";
import { Fixture } from "@modules/fixtures/domain/fixture";
import { FixtureId } from "@modules/fixtures/domain/fixtureId";
import { UniqueEntityID } from "@shared/domain/UniqueEntityID";
import { GenerateFixtureLinkDTO } from "./GenerateLinkDTO";
import { FixtureLink } from "@modules/fixtures/domain/fixtureLink";
import { LinkGeneratorService } from "@modules/fixtures/domain/services/linkGeneratorService";
import { DuplicateModelError } from "@random-guys/bucket";

type Response = Either<
  | FixtureErrors.FixtureAlreadyHasLinkError
  | FixtureErrors.FixtureNotFoundError
  | AppError.UnexpectedError
  | Result<any>,
  Result<Fixture>
>;

export class GenerateLink
  implements UseCase<GenerateFixtureLinkDTO, Promise<Response>>
{
  private repo: IFixtureRepo;

  constructor(repo: IFixtureRepo) {
    this.repo = repo;
  }

  public async execute(request: GenerateFixtureLinkDTO): Promise<Response> {
    let fixture: Fixture;

    const fixtureIdOrError = FixtureId.create(new UniqueEntityID(request.id));
    if (!fixtureIdOrError.isSuccess) return left(fixtureIdOrError);
    try {
      fixture = (await this.repo.getFixtureById(
        fixtureIdOrError.getValue()
      )) as Fixture;
    } catch (err) {
      return left(new FixtureErrors.FixtureNotFoundError(request.id));
    }

    const sanitizeLink =
      request.link && request.link.trim().length
        ? request.link
        : LinkGeneratorService.generateLink(fixture);
    const linkOrError = FixtureLink.create(
      { url: sanitizeLink as any as string },
      true
    );

    if (linkOrError.isFailure) return left(linkOrError);

    const updateLinkOrError = fixture.updateLink(linkOrError.getValue());

    if (updateLinkOrError.isLeft()) return left(updateLinkOrError.value);

    try {
      await this.repo.save(fixture);
    } catch (err) {
      if (err instanceof DuplicateModelError)
        return left(
          new FixtureErrors.FixtureLinkAlreadyExistsError(
            fixture.link?.url || ""
          )
        );
      throw err;
    }

    return right(Result.ok<Fixture>(fixture));
  }
}
