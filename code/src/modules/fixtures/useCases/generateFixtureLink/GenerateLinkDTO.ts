export interface GenerateFixtureLinkDTO {
  link?: string;
  id: string;
}
