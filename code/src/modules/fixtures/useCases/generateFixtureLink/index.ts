import { repo } from "../../repos";
import { GenerateLink } from "./GenerateLink";
import { GenerateFixtureLinkController } from "./GenerateLinkController";

const generateFixtureLink = new GenerateLink(repo);
const generateFixtureLinkController = new GenerateFixtureLinkController(
  generateFixtureLink
);

export { generateFixtureLink, generateFixtureLinkController };
