import { BaseController } from "@shared/infrastructure/http/controllers/Base";
import express from "express";
import { GenerateLink } from "./GenerateLink";
import { GenerateFixtureLinkDTO } from "./GenerateLinkDTO";
import * as FixtureErrors from "@modules/fixtures/core/FixtureErrors";

export class GenerateFixtureLinkController extends BaseController {
  private useCase: GenerateLink;

  constructor(useCase: GenerateLink) {
    super();
    this.useCase = useCase;
  }

  async executeImpl(req: express.Request, res: express.Response): Promise<any> {
    const dto: GenerateFixtureLinkDTO = {
      link: req.body.link,
      id: req.params.id,
    };

    try {
      const result = await this.useCase.execute(dto);
      if (result.isLeft()) {
        const error = result.value;
        switch (error.constructor) {
          case FixtureErrors.FixtureLinkAlreadyExistsError:
            return this.conflict(res, error.errorValue().message);
          case FixtureErrors.FixtureAlreadyHasLinkError:
            return this.conflict(res, error.errorValue().message);
          case FixtureErrors.FixtureNotFoundError:
            return this.notFound(res, error.errorValue().message);
          default:
            return this.fail(res, error.errorValue().message);
        }
      } else {
        return this.ok<void>(res);
      }
    } catch (err) {
      return this.fail(res, err);
    }
  }
}
