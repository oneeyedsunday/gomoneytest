export interface CreateFixtureDTO {
  link?: string;
  homeTeam?: string;
  startDate: string;
  teams: [string, string];
}
