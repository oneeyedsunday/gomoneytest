import * as FixtureErrors from "@modules/fixtures/core/FixtureErrors";
import { Fixture } from "@modules/fixtures/domain/fixture";
import { FixtureDate } from "@modules/fixtures/domain/fixtureDate";
import { FixtureLink } from "@modules/fixtures/domain/fixtureLink";
import { FixtureTeams } from "@modules/fixtures/domain/fixtureTeams";
import { IFixtureRepo } from "@modules/fixtures/repos/fixtureRepo";
import { DuplicateModelError } from "@random-guys/bucket";
import * as AppError from "@shared/core/AppError";
import { Either, Result, left, right } from "@shared/core/Result";
import { UseCase } from "@shared/core/UseCase";
import { UniqueEntityID } from "@shared/domain/UniqueEntityID";
import { CreateFixtureDTO } from "./CreateFixtureDTO";
import { FixtureLinkAlreadyExistsError } from "@modules/fixtures/core/FixtureErrors";

type Response = Either<
  | AppError.UnexpectedError
  | FixtureErrors.FixtureLinkAlreadyExistsError
  | Result<any>,
  Result<void>
>;

export class CreateFixture
  implements UseCase<CreateFixtureDTO, Promise<Response>>
{
  private repo: IFixtureRepo;

  constructor(repo: IFixtureRepo) {
    this.repo = repo;
  }

  public async execute(request: CreateFixtureDTO): Promise<Response> {
    try {
      const sanitizeLink =
        request.link && request.link.trim().length ? request.link : null;
      const linkOrError = FixtureLink.create(
        { url: sanitizeLink as any as string },
        true
      );

      if (linkOrError.isFailure) return left(linkOrError);

      const teamsOrError = FixtureTeams.create({
        teams: request.teams?.map((id) => new UniqueEntityID(id)) as any,
        ...(request.homeTeam
          ? { homeTeam: new UniqueEntityID(request.homeTeam) }
          : {}),
      });

      if (teamsOrError.isFailure) {
        return left(teamsOrError);
      }

      const dateOrError = FixtureDate.create({ date: request.startDate });

      if (dateOrError.isFailure) {
        return left(dateOrError);
      }

      const fixtureOrError = Fixture.create({
        date: dateOrError.getValue(),
        link: linkOrError.getValue(),
        teams: teamsOrError.getValue(),
      });

      if (fixtureOrError.isFailure) return left(fixtureOrError);

      await this.repo.save(fixtureOrError.getValue());

      return right(Result.ok<void>());
    } catch (err) {
      if (err instanceof DuplicateModelError)
        return left(new FixtureLinkAlreadyExistsError(request.link || ""));
      return left(new AppError.UnexpectedError(err));
    }
  }
}
