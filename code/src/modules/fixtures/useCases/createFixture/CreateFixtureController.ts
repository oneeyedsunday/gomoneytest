import {
  FixtureLinkAlreadyExistsError,
  HomeTeamNotInTeamsError,
} from "@modules/fixtures/core/FixtureErrors";
import { BaseController } from "@shared/infrastructure/http/controllers/Base";
import express from "express";
import { CreateFixture } from "./CreateFixture";
import { CreateFixtureDTO } from "./CreateFixtureDTO";

export class CreateFixtureController extends BaseController {
  private useCase: CreateFixture;

  constructor(useCase: CreateFixture) {
    super();
    this.useCase = useCase;
  }

  async executeImpl(req: express.Request, res: express.Response): Promise<any> {
    const dto: CreateFixtureDTO = {
      link: req.body.link,
      startDate: req.body.date,
      teams: req.body.teams,
      homeTeam: req.body.homeTeam,
    };

    try {
      const result = await this.useCase.execute(dto);

      if (result.isLeft()) {
        const error = result.value;

        switch (error.constructor) {
          case FixtureLinkAlreadyExistsError:
            return this.conflict(
              res,
              error.errorValue().message || error.errorValue()
            );
          case HomeTeamNotInTeamsError:
            return this.clientError(
              res,
              error.errorValue().message || error.errorValue()
            );
          default:
            return this.fail(
              res,
              error.errorValue().message || error.errorValue()
            );
        }
      } else {
        return this.ok(res);
      }
    } catch (err) {
      return this.fail(res, err);
    }
  }
}
