import { CreateFixture } from "./CreateFixture";
import { repo } from "../../repos";
import { CreateFixtureController } from "./CreateFixtureController";

const createFixture = new CreateFixture(repo);
const createFixtureController = new CreateFixtureController(createFixture);

export { createFixture, createFixtureController };
