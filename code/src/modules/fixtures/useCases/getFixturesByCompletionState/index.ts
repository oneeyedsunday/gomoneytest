import { repo } from "../../repos";
import { GetFixturesByCompletionState } from "./GetFixturesByCompletionState";
import { GetFixturesByCompletionStateController } from "./GetFixturesByCompletionStateController";

const getFixturesByCompletionState = new GetFixturesByCompletionState(repo);
const getFixturesByCompletionStateController =
  new GetFixturesByCompletionStateController(getFixturesByCompletionState);

export { getFixturesByCompletionState, getFixturesByCompletionStateController };
