import { Fixture } from "@modules/fixtures/domain/fixture";
import { IFixtureRepo } from "@modules/fixtures/repos/fixtureRepo";
import * as AppError from "@shared/core/AppError";
import { Either, Result, right, left } from "@shared/core/Result";
import { UseCase } from "@shared/core/UseCase";
import { IFixturePaginationOptions } from "@shared/domain/PaginationOptions";

type Response = Either<AppError.UnexpectedError, Result<Fixture[]>>;

export class GetFixturesByCompletionState
  implements UseCase<IFixturePaginationOptions, Promise<Response>>
{
  private repo: IFixtureRepo;

  constructor(repo: IFixtureRepo) {
    this.repo = repo;
  }

  public async execute(req: IFixturePaginationOptions): Promise<Response> {
    try {
      const fixtures = await this.repo.getFixtures(req);
      return right(Result.ok<Fixture[]>(fixtures));
    } catch (err) {
      return left(new AppError.UnexpectedError(err));
    }
  }
}
