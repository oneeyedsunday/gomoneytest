import { GetFixturesResponseDTO } from "@modules/fixtures/dtos/fixturesResponseDTO";
import { FixtureMap } from "@modules/fixtures/mappers/Fixture";
import {
  IFixturePaginationOptions,
  PaginationOptions,
} from "@shared/domain/PaginationOptions";
import { BaseController } from "@shared/infrastructure/http/controllers/Base";
import express from "express";
import { GetFixturesByCompletionState } from "./GetFixturesByCompletionState";
import { LoggerImpl } from "@shared/infrastructure/logger";
import { getFixturesWriteAsideCache } from "@modules/fixtures/infra/cache";

export class GetFixturesByCompletionStateController extends BaseController {
  private useCase: GetFixturesByCompletionState;

  constructor(useCase: GetFixturesByCompletionState) {
    super();
    this.useCase = useCase;
  }

  async executeImpl(req: express.Request, res: express.Response): Promise<any> {
    const isCompleted =
      ((req.query.completed as string) || "").toLowerCase() === "true";
    const dto: IFixturePaginationOptions = PaginationOptions.FromObject(
      (req.query as { [key: string]: string | number }) || {}
    );

    Object.assign(dto, { isCompleted });

    try {
      const fixtures = await getFixturesWriteAsideCache.getData(dto);
      if (fixtures) {
        LoggerImpl.info(
          `[Cache]: Cache hit for ${
            getFixturesWriteAsideCache.constructor.name
          } on query ${JSON.stringify(dto)}`
        );
        return this.ok<GetFixturesResponseDTO>(res, {
          fixtures: fixtures.map((f) => FixtureMap.toDTOFromSerialized(f)),
        });
      }
      const result = await this.useCase.execute(dto);

      if (result.isLeft()) {
        const error = result.value;

        switch (error.constructor) {
          default:
            return this.fail(res, error.errorValue().message);
        }
      } else {
        const fixtures = result.value.getValue();
        await getFixturesWriteAsideCache.setData(fixtures, dto);
        return this.ok<GetFixturesResponseDTO>(res, {
          fixtures: fixtures.map((f) => FixtureMap.toDTO(f)),
        });
      }
    } catch (err) {
      return this.fail(res, err);
    }
  }
}
