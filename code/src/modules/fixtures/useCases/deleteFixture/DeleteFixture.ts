import { IFixtureRepo } from "@modules/fixtures/repos/fixtureRepo";
import { left, right, Result, Either } from "@shared/core/Result";
import { UseCase } from "@shared/core/UseCase";
import { DeleteFixtureDTO } from "./DeleteFixtureDTO";
import * as AppError from "@shared/core/AppError";
import { FixtureId } from "@modules/fixtures/domain/fixtureId";
import { UniqueEntityID } from "@shared/domain/UniqueEntityID";

type Response = Either<AppError.UnexpectedError, Result<void>>;

export class DeleteFixtureUseCase
  implements UseCase<DeleteFixtureDTO, Promise<Response>>
{
  private repo: IFixtureRepo;

  constructor(repo: IFixtureRepo) {
    this.repo = repo;
  }

  public async execute(request: DeleteFixtureDTO): Promise<any> {
    const fixtureIdOrError = FixtureId.create(new UniqueEntityID(request.id));

    if (!fixtureIdOrError.isSuccess) return left(fixtureIdOrError);
    try {
      const fixture = await this.repo.getFixtureById(
        fixtureIdOrError.getValue(),
        true
      );
      const fixtureFound = !!fixture === true;

      if (!fixtureFound) {
        return right(Result.ok<void>());
      }

      fixture!.delete();

      await this.repo.delete(fixture!.fixtureId);

      return right(Result.ok<void>());
    } catch (err) {
      return left(new AppError.UnexpectedError(err));
    }
  }
}
