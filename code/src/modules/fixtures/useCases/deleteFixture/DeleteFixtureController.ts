import { BaseController } from "@shared/infrastructure/http/controllers/Base";
import express from "express";
import { DeleteFixtureUseCase } from "./DeleteFixture";
import { DeleteFixtureDTO } from "./DeleteFixtureDTO";

export class DeleteFixtureController extends BaseController {
  private useCase: DeleteFixtureUseCase;

  constructor(useCase: DeleteFixtureUseCase) {
    super();
    this.useCase = useCase;
  }

  async executeImpl(req: express.Request, res: express.Response): Promise<any> {
    const dto: DeleteFixtureDTO = {
      id: req.params.id,
    };

    try {
      const result = await this.useCase.execute(dto);

      if (result.isLeft()) {
        const error = result.value;

        switch (error.constructor) {
          default:
            return this.fail(res, error.errorValue().message);
        }
      } else {
        return this.ok(res);
      }
    } catch (err) {
      return this.fail(res, err);
    }
  }
}
