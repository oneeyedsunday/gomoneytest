import { repo } from "../../repos";
import { DeleteFixtureUseCase } from "./DeleteFixture";
import { DeleteFixtureController } from "./DeleteFixtureController";

const deleteFixtureUseCase = new DeleteFixtureUseCase(repo);
const deleteFixtureController = new DeleteFixtureController(
  deleteFixtureUseCase
);

export { deleteFixtureUseCase, deleteFixtureController };
