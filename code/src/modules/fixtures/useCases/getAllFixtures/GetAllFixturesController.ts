import { GetFixturesResponseDTO } from "@modules/fixtures/dtos/fixturesResponseDTO";
import { getFixturesWriteAsideCache } from "@modules/fixtures/infra/cache";
import { FixtureMap } from "@modules/fixtures/mappers/Fixture";
import {
  IPaginationOptions,
  PaginationOptions,
} from "@shared/domain/PaginationOptions";
import { BaseController } from "@shared/infrastructure/http/controllers/Base";
import { LoggerImpl } from "@shared/infrastructure/logger";
import express from "express";
import { GetAllFixtures } from "./GetAllFixtures";

export class GetAllFixturesController extends BaseController {
  private useCase: GetAllFixtures;

  constructor(useCase: GetAllFixtures) {
    super();
    this.useCase = useCase;
  }

  async executeImpl(req: express.Request, res: express.Response): Promise<any> {
    const dto: IPaginationOptions = PaginationOptions.FromObject(
      (req.query as { [key: string]: string | number }) || {}
    );

    try {
      const fixtures = await getFixturesWriteAsideCache.getData(dto);
      if (fixtures) {
        LoggerImpl.info(
          `[Cache]: Cache hit for ${
            getFixturesWriteAsideCache.constructor.name
          } on query ${JSON.stringify(dto)}`
        );
        return this.ok<GetFixturesResponseDTO>(res, {
          fixtures: fixtures.map((f) => FixtureMap.toDTOFromSerialized(f)),
        });
      }
      const result = await this.useCase.execute(dto);

      if (result.isLeft()) {
        const error = result.value;

        switch (error.constructor) {
          default:
            return this.fail(res, error.errorValue().message);
        }
      } else {
        const fixtures = result.value.getValue();
        await getFixturesWriteAsideCache.setData(fixtures, dto);
        return this.ok<GetFixturesResponseDTO>(res, {
          fixtures: fixtures.map((f) => FixtureMap.toDTO(f)),
        });
      }
    } catch (err) {
      return this.fail(res, err);
    }
  }
}
