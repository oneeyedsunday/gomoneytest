import { repo } from "../../repos";
import { GetAllFixtures } from "./GetAllFixtures";
import { GetAllFixturesController } from "./GetAllFixturesController";

const getAllFixtures = new GetAllFixtures(repo);
const getAllFixturesController = new GetAllFixturesController(getAllFixtures);

export { getAllFixtures, getAllFixturesController };
