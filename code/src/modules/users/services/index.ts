import { redisConnection } from "@shared/infrastructure/services/redis/connection";
import { RedisAuthService } from "./redisAuthService";

const authService = new RedisAuthService(redisConnection);

export { authService };
