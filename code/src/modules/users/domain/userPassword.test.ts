import { UserPassword } from "./userPassword";

describe("Users:Domain:UserPassword", () => {
  it("validate empty password", () => {
    expect(UserPassword.create({ value: "" }).isFailure).toBeTruthy();
    expect(
      UserPassword.create({ value: null as any as string }).isFailure
    ).toBeTruthy();
  });

  it("validates minLength", () => {
    expect(UserPassword.create({ value: "s" }).isFailure).toBeTruthy();
  });

  it("validated valid password", () => {
    expect(
      UserPassword.create({ value: "foogaziiiiiii" }).isSuccess
    ).toBeTruthy();
  });

  it("can get hashed values", async () => {
    const password = "sjsjsjsjsjsjsjjs";

    const userPassword = UserPassword.create({ value: password }).getValue();
    expect(userPassword.isAlreadyHashed()).toBeFalsy();

    expect(userPassword.comparePassword(password)).toBeTruthy();

    const hashed = await userPassword.getHashedValue();
    expect(hashed === password).toBeFalsy();
  });

  it("compares hashed to plaintext correctly", async () => {
    const password = "sjsjsjsjsjsjsjjs";

    const userPassword = UserPassword.create({ value: password }).getValue();
    const hashedPassword = await userPassword.getHashedValue();

    const fromHashed = UserPassword.create({
      value: hashedPassword,
      hashed: true,
    }).getValue();

    expect(await fromHashed.comparePassword(password)).toBeTruthy();
  });

  it("gets correct hashed value if already hashed", async () => {
    const password = "sjsjsjsjsjsjsjjs";

    const userPassword = UserPassword.create({ value: password }).getValue();
    const hashedPassword = await userPassword.getHashedValue();

    const fromHashed = UserPassword.create({
      value: hashedPassword,
      hashed: true,
    }).getValue();

    expect(await fromHashed.getHashedValue()).toEqual(hashedPassword);
  });
});
