import { User } from "./user";
import { UserEmail } from "./userEmail";
import { UserName } from "./userName";
import { UserPassword } from "./userPassword";

describe("Users:Domain:User", () => {
  it("validates", () => {
    expect(
      User.create({
        username: UserName.create({ name: "foo" }).getValue(),
        email: null as unknown as UserEmail,
        password: UserPassword.create({ value: "skkskskskss" }).getValue(),
      }).isFailure
    ).toBeTruthy();
  });

  it("creates", () => {
    expect(
      User.create({
        username: UserName.create({ name: "foo" }).getValue(),
        email: UserEmail.create("foo@example.com").getValue(),
        password: UserPassword.create({ value: "skkskskskss" }).getValue(),
      }).isSuccess
    ).toBeTruthy();
  });

  it("emits creation event at creation", () => {
    const user = User.create({
      username: UserName.create({ name: "foo" }).getValue(),
      email: UserEmail.create("foo@example.com").getValue(),
      password: UserPassword.create({ value: "skkskskskss" }).getValue(),
    });

    expect(user.getValue().domainEvents.length).toEqual(1);
  });

  it("can set tokens", () => {
    const user = User.create({
      username: UserName.create({ name: "foo" }).getValue(),
      email: UserEmail.create("foo@example.com").getValue(),
      password: UserPassword.create({ value: "skkskskskss" }).getValue(),
    }).getValue();

    user.setAccessToken("foo", "bar");
    expect(user.accessToken).toEqual("foo");
    expect(user.refreshToken).toEqual("bar");
  });

  it("yields internals", () => {
    const name = "foo";
    const email = "foo@example.com";
    const password = "skskskskskss";
    const user = User.create({
      username: UserName.create({ name }).getValue(),
      email: UserEmail.create(email).getValue(),
      password: UserPassword.create({ value: password }).getValue(),
    }).getValue();

    expect(user.isLoggedIn()).toBeFalsy();

    user.setAccessToken("foo", "bar");

    expect(user.userId.id).toBeTruthy();
    expect(user.accessToken).toEqual("foo");
    expect(user.email.value).toEqual(email);
    expect(user.username.value).toEqual(name);
    expect(user.isAdminUser).toBeFalsy();
    expect(user.refreshToken).toEqual("bar");
    expect(user.password.value).toEqual(password);

    expect(user.isLoggedIn()).toBeTruthy();
  });
});
