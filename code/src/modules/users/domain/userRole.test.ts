import { Role, UserRole } from "./userRole";

describe("Users:Domain:UserRole", () => {
  it("sets default to user if none provided", () => {
    const role = UserRole.create().getValue();
    expect(role.value).toEqual("user");
  });

  it("validates", () => {
    const roleOrError = UserRole.create("foo" as Role);
    expect(roleOrError.isFailure).toBeTruthy();
  });

  it("sets to admin if provided", () => {
    const role = UserRole.create("admin").getValue();
    expect(role.value).toEqual("admin");
  });

  it("transfroms to lowerCase", () => {
    const role = UserRole.create("User" as Role).getValue();
    expect(role.value).toEqual("user");
  });
});
