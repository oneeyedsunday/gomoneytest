import { UniqueEntityID } from "@shared/domain/UniqueEntityID";
import { UserId } from "./userId";

describe("Users:Domain:UserId", () => {
  it("create UserId and fills even if no id provided", () => {
    const userId = UserId.create();
    expect(userId.isSuccess).toBeTruthy();
    expect(!!userId.getValue().id).toBeTruthy();
  });

  it("create UserId from id", () => {
    const id = new UniqueEntityID("foo");
    const userId = UserId.create(id);
    expect(userId.isSuccess).toBeTruthy();
    expect(!!userId.getValue().id).toBeTruthy();
    expect(userId.getValue().id.toString()).toEqual("foo");
  });
});
