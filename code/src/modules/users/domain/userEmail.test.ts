import { UserEmail } from "./userEmail";

describe("Users:Domain:UserEmmail", () => {
  it("validates empy email", () => {
    expect(UserEmail.create("").isFailure).toBeTruthy();
  });

  it("validates invalid email address", () => {
    expect(UserEmail.create("foogazi").isFailure).toBeTruthy();
  });

  it("accepts correct email", () => {
    expect(UserEmail.create("foo@example.com").isSuccess).toBeTruthy();
  });

  it("returns value if valid", () => {
    const rawEmail = "foo@example.com";
    expect(UserEmail.create(rawEmail).getValue().value).toEqual(rawEmail);
  });

  it("formats email as lowercase", () => {
    const rawEmail = "foo@Example.com";
    expect(UserEmail.create(rawEmail).getValue().value).toEqual(
      rawEmail.toLowerCase()
    );
  });
});
