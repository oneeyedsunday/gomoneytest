import { User } from "@modules/users/domain/user";
import { UserMap } from "@modules/users/mappers/User";
import { ObjectID } from "mongodb";
import { UserCreated } from "./userCreated";

describe("Users:Domain:Events:UserCreated", () => {
  it("sanity checks for user created event", async () => {
    const _id = new ObjectID();
    const user: User = UserMap.toDomain({
      _id: _id.toHexString(),
      username: "foo",
      email: "foo@example.com",
      password: "password",
    });
    const now = Date.now();
    const event = new UserCreated(user);
    expect(event.user).toEqual(user);
    expect(event.dateTimeOccurred.valueOf()).toBeGreaterThanOrEqual(now);
    expect(event.getAggregateId().toValue()).toEqual(_id.toHexString());
  });
});
