import { UserName } from "./userName";

describe("Users:Domain:UserName", () => {
  it("validates against null values", () => {
    expect(
      UserName.create({ name: null as any as string }).isFailure
    ).toBeTruthy();
  });
  it("validate min length", () => {
    expect(UserName.create({ name: "f" }).isFailure).toBeTruthy();
  });
  it("validates max length", () => {
    const name = Array(UserName.maxLength + 10)
      .fill((Math.random() * 10).toString())
      .join("");
    expect(
      UserName.create({
        name,
      }).isFailure
    ).toBeTruthy();
  });

  it("allows if valid username", () => {
    const name = "foo";
    const userName = UserName.create({ name });
    expect(userName.isSuccess).toBeTruthy();
    expect(userName.getValue().value).toEqual(name);
  });
});
