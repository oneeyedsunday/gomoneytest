import { Result } from "@shared/core/Result";
import { ValueObject } from "@shared/domain/ValueObject";

export type Role = "admin" | "user";

export interface UserRoleProps {
  role: Role;
}

export class UserRole extends ValueObject<UserRoleProps> {
  get value(): Role {
    return this.props.role;
  }

  private constructor(props: UserRoleProps) {
    super(props);
  }

  public static create(role?: Role): Result<UserRole> {
    if (role && !["admin", "user"].includes(role.toLowerCase())) {
      return Result.fail<UserRole>(
        "Role is not valid. Valid roles are: 'admin', 'user'"
      );
    }

    return Result.ok<UserRole>(
      new UserRole({ role: role ? (role.toLowerCase() as Role) : "user" })
    );
  }
}
