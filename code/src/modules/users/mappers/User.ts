import { UniqueEntityID } from "@shared/domain/UniqueEntityID";
import { Mapper } from "@shared/infrastructure/Mapper";
import { UserRole } from "@modules/users/domain/userRole";
import { User } from "../domain/user";
import { UserEmail } from "../domain/userEmail";
import { UserName } from "../domain/userName";
import { UserPassword } from "../domain/userPassword";
import { UserDTO } from "../dtos/userDTO";

export class UserMap implements Mapper<User, UserDTO> {
  toDomain(raw: any): User {
    return UserMap.toDomain(raw);
  }

  toDTO(from: User): UserDTO {
    return UserMap.toDTO(from);
  }

  toDTOFromSerialized(raw: any): UserDTO {
    throw new Error("Not Implemented");
  }

  public static toDTO(user: User): UserDTO {
    return {
      username: user.username.value,
      isAdmin: user.isAdminUser,
    };
  }

  public static toDomain(raw: any): User {
    const userNameOrError = UserName.create({ name: raw.username });
    const userRoleOrError = UserRole.create((raw.roles || [])[0]);
    const userPasswordOrError = UserPassword.create({
      value: raw.password,
      hashed: true,
    });
    const userEmailOrError = UserEmail.create(raw.email);

    const userOrError = User.create(
      {
        username: userNameOrError.getValue(),
        roles: [userRoleOrError.getValue()],
        password: userPasswordOrError.getValue(),
        email: userEmailOrError.getValue(),
      },
      new UniqueEntityID(raw._id)
    );

    // userOrError.isFailure ? console.log(userOrError.error) : '';

    return (userOrError.isSuccess ? userOrError.getValue() : null) as User;
  }

  public static async toPersistence(user: User): Promise<any> {
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    let password: string = null;
    if (!!user.password === true) {
      if (user.password.isAlreadyHashed()) {
        password = user.password.value;
      } else {
        password = await user.password.getHashedValue();
      }
    }

    return {
      _id: user.userId.id.toString(),
      email: user.email.value,
      username: user.username.value,
      password: password,
      roles: User.GetUserRoles(user),
    };
  }
}
