import { User } from "@modules/users/domain/user";
import { UserEmail } from "@modules/users/domain/userEmail";
import { UserName } from "@modules/users/domain/userName";
import { UserPassword } from "@modules/users/domain/userPassword";
import { UserMap } from "@modules/users/mappers/User";
import { UserRole } from "../domain/userRole";

describe("Users:Mappers:User", () => {
  it("maps toDomain", () => {
    const asDomain = UserMap.toDomain({
      password: "password",
      email: "foo@example.com",
      username: "foo",
    });

    expect(asDomain).toHaveProperty("password");
    expect(asDomain).toHaveProperty("username");
    expect(asDomain).toHaveProperty("email");
  });

  it("maps to Persistence", async () => {
    const asDomain = UserMap.toDomain({
      password: "password",
      email: "foo@example.com",
      username: "foo",
    });
    const asPersistence = await UserMap.toPersistence(asDomain);

    expect(asPersistence).toHaveProperty("password");
    expect(asPersistence).toHaveProperty("roles");
    expect(asPersistence).toHaveProperty("username");
    expect(asPersistence).toHaveProperty("email");
    expect(asPersistence).toHaveProperty("_id");
    expect(asPersistence.roles.length).toEqual(1);
    expect(asPersistence.roles[0]).toEqual("user");
  });

  it("maps to DTO", () => {
    const name = "foo";
    const email = "foo@example.com";
    const password = "skskskskskss";
    const user = User.create({
      username: UserName.create({ name }).getValue(),
      email: UserEmail.create(email).getValue(),
      password: UserPassword.create({ value: password }).getValue(),
    }).getValue();

    const asDTO = UserMap.toDTO(user);
    expect(asDTO.isAdmin).toBeFalsy();
    expect(asDTO.username).toEqual(name);
  });

  it("maps admin to DTO", () => {
    const name = "foo";
    const email = "foo@example.com";
    const password = "skskskskskss";
    const user = User.create({
      username: UserName.create({ name }).getValue(),
      email: UserEmail.create(email).getValue(),
      password: UserPassword.create({ value: password }).getValue(),
      roles: [UserRole.create("admin").getValue()],
    }).getValue();

    const asDTO = UserMap.toDTO(user);
    expect(asDTO.isAdmin).toBeTruthy();
    expect(asDTO.username).toEqual(name);
  });
});
