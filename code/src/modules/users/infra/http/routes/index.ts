import { loginController } from "@modules/users/useCases/login";
import { signupController } from "@modules/users/useCases/signup";
import express from "express";

const userRouter = express.Router();

userRouter.post("/signup", (req, res) => signupController.execute(req, res));
userRouter.post("/login", (req, res) => loginController.execute(req, res));

export { userRouter };
