import { Either, left, Result, right } from "@shared/core/Result";
import * as SignupErrors from "./SignupErrors";
import * as AppError from "@shared/core/AppError";
import { User } from "@modules/users/domain/user";
import { UserEmail } from "@modules/users/domain/userEmail";
import { UserName } from "@modules/users/domain/userName";
import { UserPassword } from "@modules/users/domain/userPassword";
import { UseCase } from "@shared/core/UseCase";
import { IUserRepo } from "@modules/users/repos/userRepo";
import { SignupDTO } from "./SignupDTO";
import { UserNotFoundError } from "@modules/users/repos/implementations/mongoUserRepo";
import { UserRole } from "@modules/users/domain/userRole";

type Response = Either<
  | SignupErrors.EmailAlreadyExistsError
  | SignupErrors.UsernameTakenError
  | AppError.UnexpectedError
  | Result<any>,
  Result<void>
>;

export class SignupUseCase implements UseCase<SignupDTO, Promise<Response>> {
  private userRepo: IUserRepo;

  constructor(userRepo: IUserRepo) {
    this.userRepo = userRepo;
  }

  async execute(request: SignupDTO): Promise<Response> {
    const emailOrError = UserEmail.create(request.email);
    const passwordOrError = UserPassword.create({ value: request.password });
    const usernameOrError = UserName.create({ name: request.username });
    const userrolesOrError = UserRole.create(request.role);

    const dtoResult = Result.combine([
      emailOrError,
      passwordOrError,
      usernameOrError,
      userrolesOrError,
    ]);

    if (dtoResult.isFailure) {
      return left(Result.fail<void>(dtoResult.error)) as Response;
    }

    const email: UserEmail = emailOrError.getValue();
    const password: UserPassword = passwordOrError.getValue();
    const username: UserName = usernameOrError.getValue();
    const role: UserRole = userrolesOrError.getValue();

    try {
      const userAlreadyExists = await this.userRepo.exists(email);

      if (userAlreadyExists) {
        return left(
          new SignupErrors.EmailAlreadyExistsError(email.value)
        ) as Response;
      }

      try {
        const alreadyCreatedUserByUserName =
          await this.userRepo.getUserByUserName(username);

        const userNameTaken = !!alreadyCreatedUserByUserName === true;

        if (userNameTaken) {
          return left(
            new SignupErrors.UsernameTakenError(username.value)
          ) as Response;
        }
      } catch (err) {
        if (!(err instanceof UserNotFoundError)) throw err;
      }

      const userOrError: Result<User> = User.create({
        email,
        password,
        username,
        roles: [role],
      });

      if (userOrError.isFailure) {
        return left(
          Result.fail<User>(userOrError!.error!.toString())
        ) as Response;
      }

      const user: User = userOrError.getValue();

      await this.userRepo.save(user);

      return right(Result.ok<void>());
    } catch (err) {
      return left(new AppError.UnexpectedError(err)) as Response;
    }
  }
}
