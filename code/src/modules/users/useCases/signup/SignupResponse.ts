import * as SignupErrors from "./SignupErrors";
import * as AppError from "@shared/core/AppError";
import { Either, Result } from "@shared/core/Result";

export type SignupResponse = Either<
  | SignupErrors.EmailAlreadyExistsError
  | SignupErrors.UsernameTakenError
  | AppError.UnexpectedError
  | Result<any>,
  Result<void>
>;
