import { userRepo } from "@modules/users/repos";
import { SignupController } from "./SignupController";
import { SignupUseCase } from "./SignupUseCase";

const signupUseCase = new SignupUseCase(userRepo);
const signupController = new SignupController(signupUseCase);

export { signupUseCase, signupController };
