import { Role } from "@modules/users/domain/userRole";
export interface SignupDTO {
  username: string;
  email: string;
  password: string;
  role?: Role;
}
