import { BaseController } from "@shared/infrastructure/http/controllers/Base";
import * as express from "express";
import { SignupDTO } from "./SignupDTO";
import { SignupUseCase } from "./SignupUseCase";
import * as SignupErrors from "./SignupErrors";
import { TextUtils } from "@shared/utils/TextUtils";

export class SignupController extends BaseController {
  private useCase: SignupUseCase;

  constructor(useCase: SignupUseCase) {
    super();
    this.useCase = useCase;
  }

  async executeImpl(req: express.Request, res: express.Response): Promise<any> {
    let dto: SignupDTO = req.body as SignupDTO;

    dto = {
      username: TextUtils.sanitize(dto.username),
      email: TextUtils.sanitize(dto.email),
      password: dto.password,
      role: dto.role,
    };

    try {
      const result = await this.useCase.execute(dto);

      if (result.isLeft()) {
        const error = result.value;
        const errorMessage = error.errorValue().message
          ? error.errorValue().message
          : error.errorValue();

        switch (error.constructor) {
          case SignupErrors.UsernameTakenError:
            return this.clientError(res, errorMessage);
          case SignupErrors.EmailAlreadyExistsError:
            return this.clientError(res, errorMessage);
          default:
            return this.clientError(res, errorMessage);
        }
      } else {
        return this.ok(res);
      }
    } catch (err) {
      return this.fail(res, err);
    }
  }
}
