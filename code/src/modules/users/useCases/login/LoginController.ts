import { DecodedExpressRequest } from "@shared/infrastructure/http/models/decodedExpressRequest";
import { BaseController } from "@shared/infrastructure/http/controllers/Base";
import express from "express";
import { LoginDTO, LoginDTOResponse } from "./LoginDTO";
import * as LoginUseCaseErrors from "./LoginErrors";
import { LoginUseCase } from "./LoginUseCase";

export class LoginController extends BaseController {
  private useCase: LoginUseCase;

  constructor(useCase: LoginUseCase) {
    super();
    this.useCase = useCase;
  }

  async executeImpl(
    req: DecodedExpressRequest,
    res: express.Response
  ): Promise<any> {
    const dto: LoginDTO = req.body as LoginDTO;

    try {
      const result = await this.useCase.execute(dto);

      if (result.isLeft()) {
        const error = result.value;

        switch (error.constructor) {
          case LoginUseCaseErrors.InvalidCredentialsError:
            return this.clientError(res, error.errorValue().message);
          default:
            return this.fail(res, error.errorValue().message);
        }
      } else {
        const dto: LoginDTOResponse =
          result.value.getValue() as LoginDTOResponse;
        return this.ok<LoginDTOResponse>(res, dto);
      }
    } catch (err) {
      return this.fail(res, err);
    }
  }
}
