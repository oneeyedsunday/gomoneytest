import { JWTToken, RefreshToken } from "@shared/domain/jwt";
export interface LoginDTOResponse {
  accessToken: JWTToken;
  refreshToken: RefreshToken;
}

export interface LoginDTO {
  username: string;
  password: string;
}
