import { LoginController } from "./LoginController";
import { LoginUseCase } from "./LoginUseCase";
import { authService } from "@modules/users/services";
import { userRepo } from "@modules/users/repos";

const loginUseCase = new LoginUseCase(userRepo, authService);
const loginController = new LoginController(loginUseCase);

export { loginController, loginUseCase };
