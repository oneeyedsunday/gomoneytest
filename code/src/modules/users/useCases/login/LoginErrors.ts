import { Result } from "@shared/core/Result";
import { UseCaseError } from "@shared/core/UseCaseError";

export class InvalidCredentialsError extends Result<UseCaseError> {
  constructor() {
    super(false, {
      message: `Invalid credentials.`,
    } as UseCaseError);
  }
}
