export interface UserDTO {
  username: string;
  isAdmin?: boolean;
}
