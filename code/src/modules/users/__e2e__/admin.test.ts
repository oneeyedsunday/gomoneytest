import request from "supertest";
import * as express from "express";
import { bootstrapApp } from "@shared/infrastructure/utils/bootstrapApp";

let app: express.Application;

beforeAll(() => {
  app = bootstrapApp();
});

const username = `foo-admin-${Date.now()}`;
const email = `${username}@example.com`;
const password = "password";

describe("Admin User end to end tests", () => {
  it("should not allow invalid email", () => {
    return request(app)
      .post("/api/auth/signup")
      .set("Accept", "application/json")

      .send({ email: "foo", username, password })
      .expect(400)
      .then((res) => {
        expect(res.body.message).toEqual("Email address not valid");
        expect(res.body.status).toEqual("error");
      });
  });
  it("should be able to sign up", () => {
    return request(app)
      .post("/api/auth/signup")
      .set("Accept", "application/json")
      .send({ email, username, password, role: "admin" })
      .expect(201)
      .then((res) => {
        expect(res.body.status).toEqual("success");
      });
  });

  it("should not allow duplicate username", () => {
    return request(app)
      .post("/api/auth/signup")
      .set("Accept", "application/json")
      .send({ email: "foo2@example.com", username, password })
      .expect(400)
      .then((res) => {
        expect(res.body.message).toEqual(
          `The username ${username} was already taken`
        );
        expect(res.body.status).toEqual("error");
      });
  });

  it("should not allow duplicate email", () => {
    return request(app)
      .post("/api/auth/signup")
      .set("Accept", "application/json")
      .send({ email, username: "foo2", password })
      .expect(400)
      .then((res) => {
        expect(res.body.message).toEqual(
          `The email ${email} associated for this account already exists`
        );
        expect(res.body.status).toEqual("error");
      });
  });

  it("should login with username", () => {
    return request(app)
      .post("/api/auth/login")
      .set("Accept", "application/json")
      .send({ username, password })
      .expect(200)
      .then((res) => {
        expect(res.body.data.accessToken).toBeTruthy();
        expect(res.body.data.refreshToken).toBeTruthy();
        expect(res.body.status).toEqual("success");
      });
  });

  it("should not log in with wrong password", () => {
    return request(app)
      .post("/api/auth/login")
      .set("Accept", "application/json")
      .send({ username, password: "badPassoe" })
      .expect(400)
      .then((res) => {
        expect(res.body.message).toEqual("Invalid credentials.");
        expect(res.body.status).toEqual("error");
      });
  });
});
