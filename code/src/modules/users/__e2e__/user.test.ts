import request from "supertest";
import * as express from "express";
import { bootstrapApp } from "@shared/infrastructure/utils/bootstrapApp";

let app: express.Application;

beforeAll(() => {
  app = bootstrapApp();
});

const username = `foo-user-${Date.now()}`;
const email = `${username}@example.com`;
const password = "password";

describe("User end to end tests", () => {
  it("should not allow invalid email", () => {
    return request(app)
      .post("/api/auth/signup")
      .set("Accept", "application/json")

      .send({ email: "foo", username, password, roles: "user" })
      .expect(400)
      .then((res) => {
        expect(res.body.message).toEqual("Email address not valid");
        expect(res.body.status).toEqual("error");
      });
  });
  it("should be able to sign up", () => {
    return request(app)
      .post("/api/auth/signup")
      .set("Accept", "application/json")
      .send({ email, username, password })
      .expect(201)
      .then((res) => {
        expect(res.body.status).toEqual("success");
      });
  });

  it("should be able to sign up by specifying user role", () => {
    const specificUsername = `${username}-specific`;
    return request(app)
      .post("/api/auth/signup")
      .set("Accept", "application/json")
      .send({
        email: `${specificUsername}@example.com`,
        username: specificUsername,
        password,
        role: "user",
      })
      .expect(201)
      .then((res) => {
        expect(res.body.status).toEqual("success");
      });
  });

  it("should login with username", () => {
    return request(app)
      .post("/api/auth/login")
      .set("Accept", "application/json")
      .send({ username, password })
      .expect(200)
      .then((res) => {
        expect(res.body.data.accessToken).toBeTruthy();
        expect(res.body.data.refreshToken).toBeTruthy();
        expect(res.body.status).toEqual("success");
      });
  });

  it("should not log in with wrong password", () => {
    return request(app)
      .post("/api/auth/login")
      .set("Accept", "application/json")
      .send({ username, password: "badPassoe" })
      .expect(400)
      .then((res) => {
        expect(res.body.message).toEqual("Invalid credentials.");
        expect(res.body.status).toEqual("error");
      });
  });
});
