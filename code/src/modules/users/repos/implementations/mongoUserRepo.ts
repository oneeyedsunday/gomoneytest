import { User } from "@modules/users/domain/user";
import { UserEmail } from "@modules/users/domain/userEmail";
import { UserName } from "@modules/users/domain/userName";
import { UserMap } from "@modules/users/mappers/User";
import { IUserRepo } from "../userRepo";
import { UserModel } from "./models/User";
import { UserSchema } from "./schemas/User";
import * as mongoose from "mongoose";
import { BaseRepository } from "@random-guys/bucket";

export class UserNotFoundError extends Error {
  message = "User not found.";
  name = "UserNotFound";
}

export class MongoUserRepo
  extends BaseRepository<UserModel>
  implements IUserRepo
{
  constructor(connection: mongoose.Connection) {
    super(connection, "User", UserSchema);
  }
  async exists(userEmail: UserEmail): Promise<boolean> {
    const baseUser = await this.model.findOne({ email: userEmail.value });
    return baseUser != null;
  }
  async getUserByUserId(userId: string): Promise<User> {
    const baseUser = await this.model.findById(userId);
    if (!baseUser) throw new UserNotFoundError();
    return UserMap.toDomain(baseUser);
  }
  async getUserByUserName(userName: string | UserName): Promise<User> {
    const baseUser = await this.model.findOne({
      username:
        userName instanceof UserName ? (<UserName>userName).value : userName,
    });

    if (!baseUser) throw new UserNotFoundError();
    return UserMap.toDomain(baseUser);
  }
  async save(user: User): Promise<void> {
    const exists = await this.exists(user.email);
    if (!exists) {
      await this.create(await UserMap.toPersistence(user));
    }
  }
}
