import { SchemaTypes, Schema } from "mongoose";
import { SchemaFactory } from "@random-guys/bucket";

export const UserSchema = new Schema(
  SchemaFactory({
    email: {
      type: SchemaTypes.String,
      trim: true,
      unique: true,
      required: true,
    },
    password: { type: SchemaTypes.String, require: true },
    username: {
      type: SchemaTypes.String,
      trim: true,
      unique: true,
      required: true,
    },
    roles: {
      type: [SchemaTypes.String],
      trim: true,
      enum: ["user", "admin"],
      lowercase: true,
      default: ["user"],
    },
  })
);
