import { Model } from "@random-guys/bucket";
import { Document } from "mongoose";

export interface UserModel extends Model {
  email: string;
  username: string;
  password: string;
  roles: string[];
}

export interface UserDoc extends Document {
  email: string;
  username: string;
  password: string;
  roles: string[];
}
