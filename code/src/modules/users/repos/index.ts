import { MongoUserRepo } from "./implementations/mongoUserRepo";
import { connection } from "@shared/infrastructure/database/mongo";

const userRepo = new MongoUserRepo(connection);

export { userRepo };
