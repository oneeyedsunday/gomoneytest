import { Team } from "../domain/team";
import { TeamName } from "../domain/teamName";
import { TeamMap } from "./Team";

describe("Teams:Mappers:Team", () => {
  it("maps toDomain", () => {
    const asDomain = TeamMap.toDomain({
      name: "Girondins De Bordeaux",
      abbreviation: "BOR",
    });

    expect(asDomain).toHaveProperty("name");
    expect(asDomain).toHaveProperty("abbreviation");
  });

  it("maps to Persistence", async () => {
    const asDomain = TeamMap.toDomain({
      name: "Girondins De Bordeaux",
      abbreviation: "BOR",
    });
    const asPersistence = await TeamMap.toPersistence(asDomain);

    expect(asPersistence).toHaveProperty("name");
    expect(asPersistence).toHaveProperty("abbreviation");
    expect(asPersistence).toHaveProperty("_id");
  });

  it("maps to DTO", () => {
    const value = "Istanbul Basasksehir";
    const team = Team.create({
      name: TeamName.create({ value }).getValue(),
    }).getValue();

    const asDTO = TeamMap.toDTO(team);
    expect(asDTO.name).toEqual(value);
    expect(asDTO.abbreviation).toEqual("IST");
  });

  it("maps admin to DTO", () => {
    const name = "Watford";
    const team = Team.create({
      name: TeamName.create({ value: name }).getValue(),
    }).getValue();

    const asDTO = TeamMap.toDTO(team);
    expect(asDTO.abbreviation).toEqual("WAT");
    expect(asDTO.name).toEqual(name);
  });
});
