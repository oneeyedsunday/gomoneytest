import { UniqueEntityID } from "@shared/domain/UniqueEntityID";
import { Mapper } from "@shared/infrastructure/Mapper";
import { Team } from "../domain/team";
import { TeamName } from "../domain/teamName";
import { TeamDTO } from "../dtos/teamDTO";

export class TeamMap implements Mapper<Team, TeamDTO> {
  toDomain(raw: any): Team {
    return TeamMap.toDomain(raw);
  }

  toDTO(from: Team): TeamDTO {
    return TeamMap.toDTO(from);
  }

  toDTOFromSerialized(raw: any): TeamDTO {
    return TeamMap.toDTOFromSerialized(raw);
  }

  public static toDTO(team: Team): TeamDTO {
    return {
      teamId: team.id.toString(),
      name: team.name.value,
      abbreviation: team.abbreviation.value,
    };
  }

  public static toDTOFromSerialized(team: any): TeamDTO {
    return {
      teamId: team._id.value,
      name: team.props.name.props.value,
      abbreviation: team.props.nonce.props.value,
    };
  }

  public static toDomain(raw: any): Team {
    const teamNameOrError = TeamName.create({ value: raw.name });

    const teamOrError = Team.create(
      {
        name: teamNameOrError.getValue(),
      },
      new UniqueEntityID(raw._id)
    );

    return (teamOrError.isSuccess ? teamOrError.getValue() : null) as Team;
  }

  public static async toPersistence(team: Team): Promise<any> {
    return {
      _id: team.id.toString(),
      abbreviation: team.abbreviation.value,
      name: team.name.value,
    };
  }
}
