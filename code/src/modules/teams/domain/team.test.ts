import { Team } from "./team";
import { TeamName } from "./teamName";

describe("Teams:Domain:Team", () => {
  it("validates", () => {
    expect(
      Team.create({
        name: null as unknown as TeamName,
      }).isFailure
    ).toBeTruthy();
  });

  it("creates", () => {
    expect(
      Team.create({
        name: TeamName.create({ value: "Stevenage" }).getValue(),
      }).isSuccess
    ).toBeTruthy();
  });

  it("emits creation event at creation", () => {
    const team = Team.create({
      name: TeamName.create({ value: "Arsenal" }).getValue(),
    });

    expect(team.getValue().domainEvents.length).toEqual(1);
  });

  it("yields internals", () => {
    const name = "FC Barcelona";
    const team = Team.create({
      name: TeamName.create({ value: name }).getValue(),
    }).getValue();

    expect(team.id).toBeTruthy();
    expect(team.name.value).toEqual(name);
    expect(team.abbreviation.value).toEqual("FCB");
  });
});
