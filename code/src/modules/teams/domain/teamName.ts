import { Guard } from "@shared/core/Guard";
import { Result } from "@shared/core/Result";
import { ValueObject } from "@shared/domain/ValueObject";

export interface TeamNameProps {
  value: string;
}

export class TeamName extends ValueObject<TeamNameProps> {
  public static readonly maxLength = 255;
  public static readonly minLength = 2;

  get value(): string {
    return this.props.value;
  }

  private constructor(props: TeamNameProps) {
    super(props);
  }

  public static create(props: TeamNameProps): Result<TeamName> {
    const teamNameResult = Guard.againstNullOrUndefined(props.value, "name");

    if (!teamNameResult.succeeded)
      return Result.fail<TeamName>(teamNameResult.message as string);
    const minLengthResult = Guard.againstAtLeast(this.minLength, props.value);
    const maxLengthResult = Guard.againstAtMost(this.maxLength, props.value);

    const validation = Guard.combine([minLengthResult, maxLengthResult]);

    if (!validation.succeeded) {
      return Result.fail<TeamName>(validation.message as string);
    }

    return Result.ok<TeamName>(new TeamName(props));
  }
}
