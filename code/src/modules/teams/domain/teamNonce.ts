import { Result } from "@shared/core/Result";
import { ValueObject } from "@shared/domain/ValueObject";
import { TeamName } from "./teamName";

export interface TeamNonceProps {
  value: string;
}

export class TeamNonce extends ValueObject<TeamNonceProps> {
  public static readonly maxLength = 255;
  public static readonly minLength = 2;
  public static nonceLength = 3;

  get value(): string {
    return this.props.value;
  }

  private constructor(props: TeamNonceProps) {
    super(props);
  }

  private static generateAbbreviation(value: string, length = 3): string {
    const words = value.split(" ");
    if (words.length >= length)
      return words
        .map((x) => x.charAt(0))
        .join("")
        .replace(/[^a-zA-Z0-9]/g, "")
        .slice(0, length)
        .toUpperCase();

    return value
      .replace(/\s+/g, "")
      .replace(/[^a-zA-Z0-9]/g, "")
      .slice(0, length)
      .toUpperCase();
  }

  public static create(props: TeamNonceProps): Result<TeamNonce> {
    const teamNameResult = TeamName.create(props);

    if (!teamNameResult.isSuccess)
      return Result.fail<TeamNonce>(teamNameResult.error as string);

    const value = TeamNonce.generateAbbreviation(
      props.value,
      TeamNonce.nonceLength
    );

    return Result.ok<TeamNonce>(new TeamNonce({ value }));
  }
}
