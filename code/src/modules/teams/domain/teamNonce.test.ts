import { TeamNonce } from "./teamNonce";

describe("Teams:Domain:TeamNonce", () => {
  it("validates against null values", () => {
    expect(
      TeamNonce.create({ value: null as any as string }).isFailure
    ).toBeTruthy();
  });
  it("validate min length", () => {
    expect(TeamNonce.create({ value: "f" }).isFailure).toBeTruthy();
  });
  it("validates max length", () => {
    const value = Array(TeamNonce.maxLength + 10)
      .fill((Math.random() * 10).toString())
      .join("");
    expect(
      TeamNonce.create({
        value,
      }).isFailure
    ).toBeTruthy();
  });

  it("allows if valid teamname", () => {
    const value = "Paris St. Germain";
    const teamNonce = TeamNonce.create({ value });
    expect(teamNonce.isSuccess).toBeTruthy();
    expect(teamNonce.getValue().value).toEqual("PSG");
  });

  it("computes a nonce even if name is less than nonce length", () => {
    const value = "As";
    TeamNonce.nonceLength = 4;
    const teamNonce = TeamNonce.create({ value });
    expect(teamNonce.isSuccess).toBeTruthy();
    expect(teamNonce.getValue().value).toEqual("AS");
  });
});
