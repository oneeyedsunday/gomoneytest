import { TeamName } from "./teamName";
import { Guard } from "@shared/core/Guard";
import { Result } from "@shared/core/Result";
import { AggregateRoot } from "@shared/domain/AggregateRoot";
import { UniqueEntityID } from "@shared/domain/UniqueEntityID";
import { TeamNonce } from "./teamNonce";
import { TeamCreated } from "./events/teamCreated";
import { TeamUpdated } from "./events/teamUpdated";
import { TeamId } from "./teamId";
import { TeamDeleted } from "./events/teamDeleted";

interface TeamProps {
  name: TeamName;
  nonce: TeamNonce;
}

export class Team extends AggregateRoot<TeamProps> {
  get teamId(): TeamId {
    return TeamId.create(this._id).getValue();
  }
  get name(): TeamName {
    return this.props.name;
  }

  get abbreviation(): TeamNonce {
    return this.props.nonce;
  }

  private constructor(props: TeamProps, id?: UniqueEntityID) {
    super(props, id);
  }

  public updateTeamName(teamName: TeamName): void {
    this.props.name = teamName;
    this.props.nonce = TeamNonce.create({ value: teamName.value }).getValue();
    this.addDomainEvent(new TeamUpdated(this));
  }

  public delete(): void {
    this.addDomainEvent(new TeamDeleted(this));
  }

  public static create(
    props: Omit<TeamProps, "nonce">,
    id?: UniqueEntityID
  ): Result<Team> {
    const guardResult = Guard.againstNullOrUndefinedBulk([
      { argument: props.name, argumentName: "teamName" },
    ]);

    if (!guardResult.succeeded)
      return Result.fail<Team>(guardResult.message as string);

    const isNewTeam = !!id == false;
    const nonce = TeamNonce.create({ value: props.name.value }).getValue();
    const team = new Team({ ...props, nonce }, id);

    if (isNewTeam) team.addDomainEvent(new TeamCreated(team));

    return Result.ok<Team>(team);
  }
}
