import { UniqueEntityID } from "@shared/domain/UniqueEntityID";
import { TeamId } from "./teamId";

describe("Teams:Domain:TeamId", () => {
  it("create TeamId and fills even if no id provided", () => {
    const teamId = TeamId.create();
    expect(teamId.isSuccess).toBeTruthy();
    expect(!!teamId.getValue().id).toBeTruthy();
  });

  it("create TeamId from id", () => {
    const id = new UniqueEntityID("foo");
    const teamId = TeamId.create(id);
    expect(teamId.isSuccess).toBeTruthy();
    expect(!!teamId.getValue().id).toBeTruthy();
    expect(teamId.getValue().id.toString()).toEqual("foo");
  });
});
