import { TeamName } from "./teamName";

describe("Teams:Domain:TeamName", () => {
  it("validates against null values", () => {
    expect(
      TeamName.create({ value: null as any as string }).isFailure
    ).toBeTruthy();
  });
  it("validate min length", () => {
    expect(TeamName.create({ value: "f" }).isFailure).toBeTruthy();
  });
  it("validates max length", () => {
    const value = Array(TeamName.maxLength + 10)
      .fill((Math.random() * 10).toString())
      .join("");
    expect(
      TeamName.create({
        value,
      }).isFailure
    ).toBeTruthy();
  });

  it("allows if valid teamname", () => {
    const value = "Paris St. Germain";
    const teamName = TeamName.create({ value });
    expect(teamName.isSuccess).toBeTruthy();
    expect(teamName.getValue().value).toEqual(value);
  });
});
