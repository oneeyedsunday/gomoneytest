import { IDomainEvent } from "@shared/domain/events/IDomainEvent";
import { UniqueEntityID } from "@shared/domain/UniqueEntityID";
import { Team } from "../team";

export class TeamDeleted implements IDomainEvent {
  public dateTimeOccurred: Date;
  public team: Team;

  constructor(team: Team) {
    this.dateTimeOccurred = new Date();
    this.team = team;
  }

  getAggregateId(): UniqueEntityID {
    return this.team.id;
  }
}
