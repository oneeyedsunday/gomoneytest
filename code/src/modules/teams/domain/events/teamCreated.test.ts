import { Team } from "../../domain/team";
import { TeamMap } from "../../mappers/Team";
import { ObjectID } from "mongodb";
import { TeamCreated } from "./teamCreated";

describe("Teams:Domain:Events:TeamCreated", () => {
  it("sanity checks for team created event", async () => {
    const _id = new ObjectID();
    const team: Team = TeamMap.toDomain({
      _id: _id.toHexString(),
      name: "Juventus",
      nonce: "JUV",
    });
    const now = Date.now();
    const event = new TeamCreated(team);
    expect(event.team).toEqual(team);
    expect(event.dateTimeOccurred.valueOf()).toBeGreaterThanOrEqual(now);
    expect(event.getAggregateId().toValue()).toEqual(_id.toHexString());
  });
});
