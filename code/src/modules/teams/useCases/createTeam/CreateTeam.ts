import { Team } from "@modules/teams/domain/team";
import { TeamName } from "@modules/teams/domain/teamName";
import { ITeamRepo } from "@modules/teams/repos/teamRepo";
import * as AppError from "@shared/core/AppError";
import { Either, left, Result, right } from "@shared/core/Result";
import { UseCase } from "@shared/core/UseCase";
import { CreateTeamDTO } from "./CreateTeamDTO";

type Response = Either<AppError.UnexpectedError | Result<any>, Result<void>>;

export class CreateTeam implements UseCase<CreateTeamDTO, Promise<Response>> {
  private repo: ITeamRepo;

  constructor(repo: ITeamRepo) {
    this.repo = repo;
  }

  public async execute(request: CreateTeamDTO): Promise<Response> {
    let name: TeamName;

    try {
      const nameOrError = TeamName.create({ value: request.name });

      if (nameOrError.isFailure) {
        return left(nameOrError);
      }

      name = nameOrError.getValue();

      const teamOrError = Team.create({ name });

      if (teamOrError.isFailure) {
        return left(teamOrError);
      }

      const team = teamOrError.getValue();

      await this.repo.save(team);

      return right(Result.ok<void>());
    } catch (err) {
      return left(new AppError.UnexpectedError(err));
    }
  }
}
