import { CreateTeam } from "./CreateTeam";
import { repo } from "../../repos";
import { CreateTeamController } from "./CreateTeamController";

const createTeam = new CreateTeam(repo);
const createTeamController = new CreateTeamController(createTeam);

export { createTeam, createTeamController };
