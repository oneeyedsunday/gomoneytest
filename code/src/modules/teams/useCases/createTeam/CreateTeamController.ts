import { BaseController } from "@shared/infrastructure/http/controllers/Base";
import express from "express";
import { CreateTeam } from "./CreateTeam";
import { CreateTeamDTO } from "./CreateTeamDTO";

export class CreateTeamController extends BaseController {
  private useCase: CreateTeam;

  constructor(useCase: CreateTeam) {
    super();
    this.useCase = useCase;
  }

  async executeImpl(req: express.Request, res: express.Response): Promise<any> {
    const dto: CreateTeamDTO = {
      name: req.body.name,
    };

    try {
      const result = await this.useCase.execute(dto);

      if (result.isLeft()) {
        const error = result.value;

        switch (error.constructor) {
          default:
            return this.fail(
              res,
              error.errorValue().message || error.errorValue()
            );
        }
      } else {
        return this.ok(res);
      }
    } catch (err) {
      return this.fail(res, err);
    }
  }
}
