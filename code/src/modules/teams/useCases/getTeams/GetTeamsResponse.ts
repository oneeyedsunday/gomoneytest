import { TeamDTO } from "@modules/teams/dtos/teamDTO";

export interface GetTeamsResponseDTO {
  teams: TeamDTO[];
}
