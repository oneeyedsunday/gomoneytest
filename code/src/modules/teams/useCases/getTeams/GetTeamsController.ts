import { getTeamsWriteAsideCache } from "@modules/teams/infra/cache";
import { TeamMap } from "@modules/teams/mappers/Team";
import {
  IPaginationOptions,
  PaginationOptions,
} from "@shared/domain/PaginationOptions";
import { BaseController } from "@shared/infrastructure/http/controllers/Base";
import { LoggerImpl } from "@shared/infrastructure/logger";
import express from "express";
import { GetTeams } from "./GetTeams";
import { GetTeamsResponseDTO } from "./GetTeamsResponse";

export class GetTeamsController extends BaseController {
  private useCase: GetTeams;

  constructor(useCase: GetTeams) {
    super();
    this.useCase = useCase;
  }

  async executeImpl(req: express.Request, res: express.Response): Promise<any> {
    const dto: IPaginationOptions = PaginationOptions.FromObject(
      (req.query as { [key: string]: string | number }) || {}
    );

    try {
      const teams = await getTeamsWriteAsideCache.getData(dto);
      if (teams) {
        LoggerImpl.info(
          `[Cache]: Cache hit for ${
            getTeamsWriteAsideCache.constructor.name
          } on query ${JSON.stringify(dto)}`
        );
        return this.ok<GetTeamsResponseDTO>(res, {
          teams: teams.map((t) => TeamMap.toDTOFromSerialized(t)),
        });
      }
      const result = await this.useCase.execute(dto);

      if (result.isLeft()) {
        const error = result.value;

        switch (error.constructor) {
          default:
            return this.fail(res, error.errorValue().message);
        }
      } else {
        const teams = result.value.getValue();
        await getTeamsWriteAsideCache.setData(teams, dto);
        return this.ok<GetTeamsResponseDTO>(res, {
          teams: teams.map((t) => TeamMap.toDTO(t)),
        });
      }
    } catch (err) {
      return this.fail(res, err);
    }
  }
}
