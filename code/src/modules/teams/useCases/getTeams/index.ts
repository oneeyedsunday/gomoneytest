import { repo } from "../../repos";
import { GetTeams } from "./GetTeams";
import { GetTeamsController } from "./GetTeamsController";

const getTeams = new GetTeams(repo);
const getTeamsController = new GetTeamsController(getTeams);

export { getTeams, getTeamsController };
