import { Team } from "@modules/teams/domain/team";
import { ITeamRepo } from "@modules/teams/repos/teamRepo";
import { Either, Result, right, left } from "@shared/core/Result";
import { UseCase } from "@shared/core/UseCase";
import * as AppError from "@shared/core/AppError";
import { IPaginationOptions } from "@shared/domain/PaginationOptions";

type Response = Either<AppError.UnexpectedError, Result<Team[]>>;

export class GetTeams
  implements UseCase<IPaginationOptions, Promise<Response>>
{
  private repo: ITeamRepo;

  constructor(repo: ITeamRepo) {
    this.repo = repo;
  }

  public async execute(req: IPaginationOptions): Promise<Response> {
    try {
      const teams = await this.repo.getTeams(req);
      return right(Result.ok<Team[]>(teams));
    } catch (err) {
      return left(new AppError.UnexpectedError(err));
    }
  }
}
