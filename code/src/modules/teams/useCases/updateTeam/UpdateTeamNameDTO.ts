export interface UpdateTeamNameDTO {
  name: string;
  id: string;
}
