import { Team } from "@modules/teams/domain/team";
import { TeamMap } from "@modules/teams/mappers/Team";
import { Either } from "@shared/core/Result";
import { BaseController } from "@shared/infrastructure/http/controllers/Base";
import express from "express";
import { UpdateTeamName } from "./UpdateTeamName";
import { UpdateTeamNameDTO } from "./UpdateTeamNameDTO";
import * as UpdateTeamErrors from "./UpdateTeamNameErrors";

export class UpdateTeamController extends BaseController {
  private useCase: UpdateTeamName;

  constructor(useCase: UpdateTeamName) {
    super();
    this.useCase = useCase;
  }

  async executeImpl(req: express.Request, res: express.Response): Promise<any> {
    const dto: UpdateTeamNameDTO = {
      name: req.body.name,
      id: req.params.id,
    };

    try {
      const result = await this.useCase.execute(dto);
      if (result.isLeft()) {
        const error = result.value;
        switch (error.constructor) {
          case UpdateTeamErrors.TeamNotFoundError:
            return this.notFound(res, error.errorValue().message);
          default:
            return this.fail(res, error.errorValue().message);
        }
      } else {
        const team = (result as Either<any, Team>).value.getValue();
        return this.ok(res, TeamMap.toDTO(team));
      }
    } catch (err) {
      return this.fail(res, err);
    }
  }
}
