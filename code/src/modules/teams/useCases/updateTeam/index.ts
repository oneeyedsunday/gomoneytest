import { repo } from "../../repos";
import { UpdateTeamController } from "./UpdateTeamController";
import { UpdateTeamName } from "./UpdateTeamName";

const updateTeam = new UpdateTeamName(repo);
const updateTeamController = new UpdateTeamController(updateTeam);

export { updateTeam, updateTeamController };
