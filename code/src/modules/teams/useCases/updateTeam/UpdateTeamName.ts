import { ITeamRepo } from "@modules/teams/repos/teamRepo";
import { Result, left, right, Either } from "@shared/core/Result";
import { UseCase } from "@shared/core/UseCase";
import { UpdateTeamNameDTO } from "./UpdateTeamNameDTO";
import * as AppError from "@shared/core/AppError";
import { TeamName } from "@modules/teams/domain/teamName";
import { Team } from "@modules/teams/domain/team";
import { TeamId } from "@modules/teams/domain/teamId";
import * as UpdateTeamNameErrors from "./UpdateTeamNameErrors";
import { UniqueEntityID } from "@shared/domain/UniqueEntityID";

type Response = Either<
  | UpdateTeamNameErrors.TeamNotFoundError
  | AppError.UnexpectedError
  | Result<any>,
  Result<Team>
>;

export class UpdateTeamName
  implements UseCase<UpdateTeamNameDTO, Promise<Response>>
{
  private repo: ITeamRepo;

  constructor(repo: ITeamRepo) {
    this.repo = repo;
  }

  public async execute(request: UpdateTeamNameDTO): Promise<Response> {
    let team: Team;

    const teamIdOrError = TeamId.create(new UniqueEntityID(request.id));

    if (!teamIdOrError.isSuccess) return left(teamIdOrError);
    try {
      team = (await this.repo.getTeamById(teamIdOrError.getValue())) as Team;
    } catch (err) {
      return left(new UpdateTeamNameErrors.TeamNotFoundError(request.id));
    }

    const teamNameOrError = TeamName.create({ value: request.name });

    if (!teamNameOrError.isSuccess) return left(teamNameOrError);

    const teamName = teamNameOrError.getValue();
    team.updateTeamName(teamName);

    await this.repo.save(team);

    return right(Result.ok<Team>(team));
  }
}
