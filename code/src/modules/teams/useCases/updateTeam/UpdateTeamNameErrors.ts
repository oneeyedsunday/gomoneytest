import { Result } from "@shared/core/Result";
import { UseCaseError } from "@shared/core/UseCaseError";

export class TeamNotFoundError extends Result<UseCaseError> {
  constructor(id: string) {
    super(false, {
      message: `Couldn't find a team by id {${id}}.`,
    } as UseCaseError);
  }
}
