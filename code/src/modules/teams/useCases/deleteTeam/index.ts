import { repo } from "../../repos";
import { DeleteTeamUseCase } from "./DeleteTeam";
import { DeleteTeamController } from "./DeleteTeamController";

const deleteTeamUseCase = new DeleteTeamUseCase(repo);
const deleteTeamController = new DeleteTeamController(deleteTeamUseCase);

export { deleteTeamUseCase, deleteTeamController };
