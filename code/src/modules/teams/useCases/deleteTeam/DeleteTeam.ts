import { ITeamRepo } from "@modules/teams/repos/teamRepo";
import { left, right, Result, Either } from "@shared/core/Result";
import { UseCase } from "@shared/core/UseCase";
import { DeleteTeamDTO } from "./DeleteTeamDTO";
import * as AppError from "@shared/core/AppError";
import { Team } from "@modules/teams/domain/team";
import { TeamId } from "@modules/teams/domain/teamId";
import { UniqueEntityID } from "@shared/domain/UniqueEntityID";

type Response = Either<AppError.UnexpectedError, Result<void>>;

export class DeleteTeamUseCase
  implements UseCase<DeleteTeamDTO, Promise<Response>>
{
  private repo: ITeamRepo;

  constructor(repo: ITeamRepo) {
    this.repo = repo;
  }

  public async execute(request: DeleteTeamDTO): Promise<any> {
    const teamIdOrError = TeamId.create(new UniqueEntityID(request.id));

    if (!teamIdOrError.isSuccess) return left(teamIdOrError);
    try {
      const team = await this.repo.getTeamById(teamIdOrError.getValue(), true);
      const teamFound = !!team === true;

      if (!teamFound) {
        return right(Result.ok<void>());
      }

      (team as Team).delete();

      await this.repo.delete(team as Team);

      return right(Result.ok<void>());
    } catch (err) {
      return left(new AppError.UnexpectedError(err));
    }
  }
}
