import request from "supertest";
import * as express from "express";
import { bootstrapApp } from "@shared/infrastructure/utils/bootstrapApp";

let app: express.Application;

beforeAll(() => {
  app = bootstrapApp();
});

const username = `teams-user-${Date.now()}`;
const email = `${username}@example.com`;
const password = "password";
let token: string;
let teamId: string;

describe("Teams end to end tests", () => {
  beforeAll(() => {
    return request(app)
      .post("/api/auth/signup")
      .set("Accept", "application/json")
      .send({ email, username, password })
      .then(() =>
        request(app)
          .post("/api/auth/login")
          .set("Accept", "application/json")
          .send({ username, password })
          .then((res) => {
            token = `Bearer ${res.body.data.accessToken}`;
          })
      );
  });

  it("user should not be able to create team", () => {
    return request(app)
      .post("/api/teams")
      .set("Accept", "application/json")
      .set("Authorization", token)
      .send({ name: "Chelsea FC" })
      .expect(403);
  });

  it("user can list teams", () => {
    return request(app)
      .get("/api/teams")
      .set("Accept", "application/json")
      .set("Authorization", token)
      .expect(200)
      .then((res) => {
        expect(res.body.status).toEqual("success");
        expect(res.body.data).toBeTruthy();
        expect(res.body.data).toHaveProperty("teams");
        expect(res.body.data.teams.length).toBeTruthy();
        expect(res.body.data.teams[0]).toHaveProperty("teamId");
        teamId = res.body.data.teams[1].teamId;
        expect(res.body.data.teams[0]).toHaveProperty("name");
        expect(res.body.data.teams[0]).toHaveProperty("abbreviation");
      });
  });

  it("user should not be able to update team", () => {
    const name = "FC Santa Clause";
    return request(app)
      .put(`/api/teams/${teamId}`)
      .set("Accept", "application/json")
      .set("Authorization", token)
      .send({ name })
      .expect(403);
  });

  it("user should not be able to delete team", () => {
    return request(app)
      .delete(`/api/teams/${teamId}`)
      .set("Accept", "application/json")
      .set("Authorization", token)
      .expect(403);
  });
});
