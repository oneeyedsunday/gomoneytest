import request from "supertest";
import * as express from "express";
import { bootstrapApp } from "@shared/infrastructure/utils/bootstrapApp";
import { repo } from "../repos";
import { TeamModel } from "../repos/models/Team";

let app: express.Application;

beforeAll(() => {
  app = bootstrapApp();
});

const username = `teams-admin-${Date.now()}`;
const email = `${username}@example.com`;
const password = "password";
let token: string;
let teamId: string;

describe("Teams end to end tests", () => {
  beforeAll(() => {
    return request(app)
      .post("/api/auth/signup")
      .set("Accept", "application/json")
      .send({ email, username, password, role: "admin" })
      .then(() =>
        request(app)
          .post("/api/auth/login")
          .set("Accept", "application/json")
          .send({ username, password })
          .then((res) => {
            token = `Bearer ${res.body.data.accessToken}`;
          })
      );
  });

  it("creates team", () => {
    return request(app)
      .post("/api/teams")
      .set("Accept", "application/json")
      .set("Authorization", token)
      .send({ name: "Chelsea FC" })
      .expect(201);
  });

  it("creates another team with same name", () => {
    return request(app)
      .post("/api/teams")
      .set("Accept", "application/json")
      .set("Authorization", token)
      .send({ name: "Chelsea FC" })
      .expect(201);
  });

  it("list teams", () => {
    return request(app)
      .get("/api/teams")
      .set("Accept", "application/json")
      .set("Authorization", token)
      .expect(200)
      .then((res) => {
        expect(res.body.status).toEqual("success");
        expect(res.body.data).toBeTruthy();
        expect(res.body.data).toHaveProperty("teams");
        expect(res.body.data.teams.length).toBeTruthy();
        expect(res.body.data.teams[0]).toHaveProperty("teamId");
        teamId = res.body.data.teams[1].teamId;
        expect(res.body.data.teams[0]).toHaveProperty("name");
        expect(res.body.data.teams[0]).toHaveProperty("abbreviation");
      });
  });

  it("handles pagination", () => {
    return request(app)
      .get("/api/teams?offset=0&limit=1")
      .set("Accept", "application/json")
      .set("Authorization", token)
      .expect(200)
      .then((res) => {
        expect(res.body.status).toEqual("success");
        expect(res.body.data).toBeTruthy();
        expect(res.body.data).toHaveProperty("teams");
        expect(res.body.data.teams.length).toEqual(1);
        expect(res.body.data.teams[0]).toHaveProperty("teamId");
        expect(res.body.data.teams[0]).toHaveProperty("name");
        expect(res.body.data.teams[0]).toHaveProperty("abbreviation");
      });
  });

  it("updates team", () => {
    const name = "FC Santa Clause";
    return request(app)
      .put(`/api/teams/${teamId}`)
      .set("Accept", "application/json")
      .set("Authorization", token)
      .send({ name })
      .expect(200)
      .then((res) => {
        expect(res.body.data).toBeTruthy();
        expect("CHE" !== res.body.data.abbreviation).toBeTruthy();
        expect(res.body.data.abbreviation).toBeTruthy();
        expect(res.body.data.name).toBe(name);
      });
  });

  it("updates team fails with 404 if team not found", () => {
    return request(app)
      .put("/api/teams/-1")
      .set("Accept", "application/json")
      .set("Authorization", token)
      .send({ name: "FC Santa Clause" })
      .expect(404);
  });

  // unreliable since caching is enabled
  // also, other tests add teams
  // so unable to say which should be first without inserting
  it("lists teams by created", () => {
    const name = "Bandwagon Marathon";
    return request(app)
      .post("/api/teams")
      .set("Accept", "application/json")
      .set("Authorization", token)
      .send({ name })
      .then(
        () =>
          request(app)
            .get("/api/teams?offset=0&limit=1")
            .set("Accept", "application/json")
            .set("Authorization", token)
        // .then((res) => {
        //   // expect(res.body.data.teams[0].name).toEqual(name);
        // })
      );
  });

  function wrapCallbackInPromise<T>(func: any, ...args: any[]): Promise<T> {
    return new Promise((resolve, reject) => {
      func(...args, (err: Error, result: T) => {
        if (err) return reject(err);
        resolve(result);
      });
    });
  }

  it("deletes a team", () => {
    // Caching means we dont get correct count
    // just grab from repo
    return Promise.all([
      wrapCallbackInPromise<number>(repo.model.countDocuments.bind(repo.model)),
      wrapCallbackInPromise<TeamModel>(repo.model.findOne.bind(repo.model), {}),
    ]).then(([count, team]) =>
      request(app)
        .delete(`/api/teams/${team._id}`)
        .set("Accept", "application/json")
        .set("Authorization", token)
        .expect(201)
        .then(() =>
          request(app)
            .get("/api/teams?offset=0&limit=5000")
            .set("Accept", "application/json")
            .set("Authorization", token)
            .then((res) => {
              expect(res.body.data.teams.length + 1).toEqual(count);
            })
        )
    );
  });
});
