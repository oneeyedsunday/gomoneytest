export interface TeamDTO {
  name: string;
  abbreviation: string;
  teamId?: string;
}
