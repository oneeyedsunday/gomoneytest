import { Model } from "@random-guys/bucket";
import { Document } from "mongoose";

export interface TeamModel extends Model {
  name: string;
  abbreviation: string;
}

export interface TeamDoc extends Document {
  name: string;
  abbreviation: string;
}
