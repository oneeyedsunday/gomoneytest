import { IPaginationOptions } from "@shared/domain/PaginationOptions";
import { Team } from "../domain/team";
import { TeamId } from "../domain/teamId";

export interface ITeamRepo {
  getTeamById(teamId: TeamId, ignoreNotFound?: boolean): Promise<Team | null>;
  getTeams(paginationOptions: IPaginationOptions): Promise<Team[]>;
  exists(teamId: TeamId): Promise<boolean>;
  save(team: Team): Promise<void>;
  delete(teamId: TeamId): Promise<void>;
}
