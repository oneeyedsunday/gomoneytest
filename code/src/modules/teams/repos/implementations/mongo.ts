import { BaseRepository } from "@random-guys/bucket";
import { ITeamRepo } from "../teamRepo";
import { Team } from "@modules/teams/domain/team";
import * as mongoose from "mongoose";
import { TeamSchema } from "../schemas/Team";
import { TeamModel } from "../models/Team";
import { TeamId } from "@modules/teams/domain/teamId";
import { IPaginationOptions } from "@shared/domain/PaginationOptions";
import { TeamMap } from "@modules/teams/mappers/Team";

export class TeamNotFoundError extends Error {
  message = "Team not found.";
  name = "TeamNotFound";
}

export class MongoTeamRepo
  extends BaseRepository<TeamModel>
  implements ITeamRepo
{
  constructor(connection: mongoose.Connection) {
    super(connection, "Team", TeamSchema);
  }
  async getTeamById(
    teamId: TeamId,
    ignoreNotFound?: boolean
  ): Promise<Team | null> {
    return ignoreNotFound
      ? this.byID(teamId.id.toString(), undefined, false).then((teamModel) => {
          if (teamModel) return TeamMap.toDomain(teamModel);
          return null;
        })
      : this.byID(teamId.id.toString())
          .then((teamModel) => TeamMap.toDomain(teamModel))
          .catch(() => {
            throw new TeamNotFoundError();
          });
  }
  async getTeams(paginationOptions: IPaginationOptions): Promise<Team[]> {
    return this.model
      .find({})
      .limit(paginationOptions.limit)
      .skip(paginationOptions.skip)
      .sort({ created_at: -1 })
      .then((teamModels) => teamModels.map((tM) => TeamMap.toDomain(tM)));
  }
  async exists(teamId: TeamId): Promise<boolean> {
    const team = await this.model.findById(teamId.id);
    return team != null;
  }
  async save(team: Team): Promise<void> {
    const isExistingTeam = await this.exists(team.teamId);

    if (!isExistingTeam) {
      await this.create(await TeamMap.toPersistence(team));
    } else {
      await this.update(team.id.toValue().toString(), {
        name: team.name.value,
        abbreviation: team.abbreviation.value,
      });
    }
  }
  async delete(teamId: TeamId): Promise<void> {
    await new Promise<void>((resolve, reject) => {
      this.model.findOneAndDelete(
        { _id: teamId.id.toString() },
        null,
        (err) => {
          if (err) return reject(err);
          resolve();
        }
      );
    });
  }
}
