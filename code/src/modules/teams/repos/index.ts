import { MongoTeamRepo } from "./implementations/mongo";
import { connection } from "@shared/infrastructure/database/mongo";
import { TeamSchema } from "./schemas/Team";
import { runHook } from "@shared/infrastructure/database/mongo/hook";

// important to call this before compiling model (in our case in the repo)
// see https://mongoosejs.com/docs/middleware.html#defining
runHook(TeamSchema);

const repo = new MongoTeamRepo(connection);

export { repo };
