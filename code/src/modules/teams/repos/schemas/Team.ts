import { SchemaTypes, Schema } from "mongoose";
import { SchemaFactory } from "@random-guys/bucket";

export const TeamSchema = new Schema(
  SchemaFactory({
    name: {
      type: SchemaTypes.String,
      trim: true,
      required: true,
    },
    abbreviation: {
      type: SchemaTypes.String,
      trim: true,
    },
  })
);
