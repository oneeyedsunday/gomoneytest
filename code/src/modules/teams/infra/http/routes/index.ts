import { createTeamController } from "@modules/teams/useCases/createTeam";
import { deleteTeamController } from "@modules/teams/useCases/deleteTeam";
import { getTeamsController } from "@modules/teams/useCases/getTeams";
import { updateTeamController } from "@modules/teams/useCases/updateTeam";
import { adminMiddleware } from "@shared/infrastructure/http";
import express from "express";
const teamRouter = express.Router();

teamRouter.get("", (req, res) => getTeamsController.execute(req, res));
teamRouter.post("", adminMiddleware.ensureIsAdmin(), (req, res) =>
  createTeamController.execute(req, res)
);
teamRouter.put("/:id", adminMiddleware.ensureIsAdmin(), (req, res) =>
  updateTeamController.execute(req, res)
);
teamRouter.delete("/:id", adminMiddleware.ensureIsAdmin(), (req, res) =>
  deleteTeamController.execute(req, res)
);

export { teamRouter };
