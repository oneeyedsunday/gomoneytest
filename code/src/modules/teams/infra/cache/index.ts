import { redisConnection } from "@shared/infrastructure/services/redis/connection";
import { GetTeamsRedisCache } from "./Team";

const ttl = Number(process.env.CACHE_TTL_SECONDS || 30);

const getTeamsWriteAsideCache = new GetTeamsRedisCache(
  redisConnection,
  ttl,
  (queryOptions) => `teams-${queryOptions.offset}-${queryOptions.limit}`
);

export { getTeamsWriteAsideCache };
