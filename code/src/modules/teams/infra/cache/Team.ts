import { Team } from "@modules/teams/domain/team";
import { IPaginationOptions } from "@shared/domain/PaginationOptions";
import safeJsonStringify from "@bugsnag/safe-json-stringify";
import { RedisWriteAsideCache } from "@shared/infrastructure/cache/RedisCache";
type GetTeamsCacheOptions = IPaginationOptions;

export class GetTeamsRedisCache extends RedisWriteAsideCache<
  Team[],
  GetTeamsCacheOptions
> {
  public async getData(options: GetTeamsCacheOptions): Promise<Team[] | null> {
    try {
      const items = await this.getAsync(this.keyFactory(options));
      return JSON.parse(items as string) as Team[];
    } catch (err) {
      return null;
    }
  }
  public async setData(
    value: Team[],
    options: GetTeamsCacheOptions
  ): Promise<void> {
    await this.setAsync(this.keyFactory(options), safeJsonStringify(value));
  }
}
