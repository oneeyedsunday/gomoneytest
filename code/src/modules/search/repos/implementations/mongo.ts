import { BaseRepository } from "@random-guys/bucket";
import {
  FixtureSearchResultData,
  SearchResultModel,
  SearchResultType,
  TeamSearchResultData,
} from "../models/SearchResult";
import { SearchResultSchema } from "../schemas/SearchResult";
import { ISearchRepo } from "../searchRepo";
import * as mongoose from "mongoose";
import { ISearchOptions } from "@shared/domain/SearchOptions";
import { UniqueEntityID } from "@shared/domain/UniqueEntityID";
import { ObjectID } from "mongodb";

export class MongoSearchResultRepo
  extends BaseRepository<SearchResultModel>
  implements ISearchRepo
{
  constructor(connection: mongoose.Connection) {
    super(connection, "SearchResult", SearchResultSchema);
  }
  getResults(
    type: SearchResultType | null,
    searchOptions: ISearchOptions
  ): Promise<SearchResultModel[]> {
    const searchRegex = searchOptions.query
      ? new RegExp(searchOptions.query, "i")
      : null;
    const filter = {
      ...(searchOptions.query
        ? {
            $or: [
              { "data.name": searchRegex },
              { "data.abbreviation": searchRegex },
              { "data.title": searchRegex },
              { "data.shortTitle": searchRegex },
            ],
          }
        : {}),
      ...(type ? { type } : {}),
    };
    return this.model
      .find(filter)
      .limit(searchOptions.limit)
      .skip(searchOptions.skip)
      .sort({ created_at: -1 })
      .then((results) => results);
  }

  async upsertTeamResult(team: TeamSearchResultData): Promise<void> {
    await new Promise<void>((resolve, reject) => {
      this.model.findOneAndUpdate(
        { "data._id": team._id, type: "team" },
        { data: team, type: "team" },
        { upsert: true },
        (err) => {
          if (err) return reject(err);
          resolve();
        }
      );
    });
  }

  async removeTeamResult(id: UniqueEntityID): Promise<void> {
    // TODO (oneeyedsunday) remove from fixtures???
    await new Promise<void>((resolve, reject) => {
      this.model.findOneAndDelete(
        { "data._id": new ObjectID(id.toString()), type: "team" },
        {},
        (err) => {
          if (err) return reject(err);
          resolve();
        }
      );
    });
  }

  async upsertFixtureResult(fixture: FixtureSearchResultData): Promise<void> {
    await new Promise<void>((resolve, reject) => {
      this.model.findOneAndUpdate(
        { "data._id": fixture._id, type: "fixture" },
        { data: fixture, type: "fixture" },
        { upsert: true },
        (err) => {
          if (err) return reject(err);
          resolve();
        }
      );
    });
  }
  async removeFixtureResult(id: UniqueEntityID): Promise<void> {
    await new Promise<void>((resolve, reject) => {
      this.model.findOneAndDelete(
        { "data._id": id.toValue().toString(), type: "fixture" },
        {},
        (err) => {
          if (err) return reject(err);
          resolve();
        }
      );
    });
  }
}
