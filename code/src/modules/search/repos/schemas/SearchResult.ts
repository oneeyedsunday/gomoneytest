import { SchemaTypes, Schema } from "mongoose";
import { SchemaFactory } from "@random-guys/bucket";

export const SearchResultSchema = new Schema(
  SchemaFactory({
    type: {
      type: SchemaTypes.String,
      enum: ["fixture", "team"],
      lowercase: true,
      trim: true,
      required: true,
    },
    data: {
      type: SchemaTypes.Mixed,
      required: true,
    },
  })
);

SearchResultSchema.index({ "data.name": "text", "data.abbreviation": "text" });
