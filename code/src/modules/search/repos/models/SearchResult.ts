import { Model } from "@random-guys/bucket";
import { ObjectID } from "mongodb";
import { Document } from "mongoose";

export type SearchResultType = "team" | "fixture";

export interface SearchResultModel extends Model {
  type: SearchResultType;
  data: SearchResultData;
}

type SearchResultData = TeamSearchResultData | FixtureSearchResultData;
type ID = string | ObjectID;

export interface TeamSearchResultData {
  _id: ID;
  name: string;
  abbreviation: string;
}

export interface FixtureSearchResultData {
  _id: ID;
  title: string;
  link: string;
  shortTitle: string;
  teams: [TeamSearchResultData, TeamSearchResultData];
  date: Date;
  homeTeam?: TeamSearchResultData;
}

export interface SearchResultDoc extends Document {
  type: SearchResultType;
  data: SearchResultData;
}
