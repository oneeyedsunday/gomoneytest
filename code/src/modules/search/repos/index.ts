import { connection } from "@shared/infrastructure/database/mongo";
import { MongoSearchResultRepo } from "./implementations/mongo";

const repo = new MongoSearchResultRepo(connection);

export { repo };
