import { ISearchOptions } from "@shared/domain/SearchOptions";
import { UniqueEntityID } from "@shared/domain/UniqueEntityID";
import {
  FixtureSearchResultData,
  SearchResultModel,
  SearchResultType,
  TeamSearchResultData,
} from "./models/SearchResult";

export interface ISearchRepo {
  getResults(
    type: SearchResultType | null,
    searchOptions: ISearchOptions
  ): Promise<SearchResultModel[]>;

  upsertTeamResult(team: TeamSearchResultData): Promise<void>;
  removeTeamResult(id: UniqueEntityID): Promise<void>;
  upsertFixtureResult(fixture: FixtureSearchResultData): Promise<void>;
  removeFixtureResult(id: UniqueEntityID): Promise<void>;
}
