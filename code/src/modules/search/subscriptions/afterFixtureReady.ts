import { FixtureLinkGenerated } from "@modules/fixtures/domain/events/fixtureLinkGenerated";
import { PopulatedFixtureModel } from "@modules/fixtures/repos/models/Fixture";
import { DomainEvents } from "@shared/domain/events/DomainEvents";
import { IHandle } from "@shared/domain/events/IHandle";
import { LoggerImpl } from "@shared/infrastructure/logger";
import { SearchResultMap } from "../mappers/SearchResult";
import { UpsertFixtureEntry } from "../useCases/upsertFixtureEntry/upsertFixtureEntry";

export class AfterFixtureLinkGenerated
  implements IHandle<FixtureLinkGenerated>
{
  private readonly upsertFixtureEntry: UpsertFixtureEntry;
  constructor(upsertFixtureEntry: UpsertFixtureEntry) {
    this.setupSubscriptions();
    this.upsertFixtureEntry = upsertFixtureEntry;
  }
  setupSubscriptions(): void {
    DomainEvents.register(
      (e, o) =>
        this.onFixtureLinkGenerated(e as FixtureLinkGenerated, o as any),
      FixtureLinkGenerated.name
    );
  }

  private async onFixtureLinkGenerated(
    event: FixtureLinkGenerated,
    options: Promise<PopulatedFixtureModel>
  ) {
    LoggerImpl.info(
      `[Search]: Received FixtureLinkGenerated Event: ${event.getAggregateId()}`
    );
    const populatedFixture = await options;
    if (
      !populatedFixture ||
      !Array.isArray(populatedFixture?.teams) ||
      populatedFixture?.teams.length !== 2
    ) {
      LoggerImpl.info(
        "Fixture is invalid, teams could not be found. Skipping processing"
      );
      return;
    }
    const title = populatedFixture.teams.map((tM) => tM.name).join(" vs ");
    try {
      await this.upsertFixtureEntry.execute(
        SearchResultMap.toFixtureSearchResult(populatedFixture)
      );
      LoggerImpl.info(
        `[Search]: Added fixture entry for #${event
          .getAggregateId()
          .toString()}: ${title}`
      );
    } catch (err) {
      LoggerImpl.error(
        `[Search]: Failed to add fixture entry for #${event
          .getAggregateId()
          .toString()}: ${title}`
      );
    }
  }
}
