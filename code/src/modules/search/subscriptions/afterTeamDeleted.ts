import { TeamDeleted } from "@modules/teams/domain/events/teamDeleted";
import { DomainEvents } from "@shared/domain/events/DomainEvents";
import { IHandle } from "@shared/domain/events/IHandle";
import { LoggerImpl } from "@shared/infrastructure/logger";
import { TeamSearchResultData } from "../repos/models/SearchResult";
import { DeleteTeamEntry } from "../useCases/removeTeamEntry/deleteTeamEntry";

export class AfterTeamDeleted implements IHandle<TeamDeleted> {
  private readonly deleteTeamEntry: DeleteTeamEntry;
  constructor(deleteTeamEntry: DeleteTeamEntry) {
    this.setupSubscriptions();
    this.deleteTeamEntry = deleteTeamEntry;
  }
  setupSubscriptions(): void {
    DomainEvents.register(
      (e) => this.onTeamDeleted(e as TeamDeleted),
      TeamDeleted.name
    );
  }

  private async onTeamDeleted(event: TeamDeleted) {
    LoggerImpl.info(
      `[Search]: Received TeamCreated Event: ${event
        .getAggregateId()
        .toString()}`
    );

    try {
      const entry: TeamSearchResultData = {
        _id: event.team.id.toValue(),
        name: event.team.name.value,
        abbreviation: event.team.abbreviation.value,
      };
      await this.deleteTeamEntry.execute(entry);
      LoggerImpl.info(
        `[Search]: Deleted team entry for #${event
          .getAggregateId()
          .toString()}: ${event.team.name.value}`
      );
    } catch (err) {
      LoggerImpl.error(
        `[Search]: Failed to delete team entry for #${event
          .getAggregateId()
          .toString()}: ${event.team.name.value}`
      );
    }
  }
}
