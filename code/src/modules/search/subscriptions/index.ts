import { deleteFixtureEntry } from "../useCases/removeFixtureEntry";
import { deleteTeamEntry } from "../useCases/removeTeamEntry";
import { upsertFixtureEntry } from "../useCases/upsertFixtureEntry";
import { upsertTeamEntry } from "../useCases/upsertTeamEntry";
import { AfterFixtureDeleted } from "./afterFixtureDeleted";
import { AfterFixtureLinkGenerated } from "./afterFixtureReady";
import { AfterFixtureUpdated } from "./afterFixtureUpdated";
import { AfterTeamCreated } from "./afterTeamCreated";
import { AfterTeamDeleted } from "./afterTeamDeleted";
import { AfterTeamUpdated } from "./afterTeamUpdated";

new AfterTeamCreated(upsertTeamEntry);
new AfterTeamUpdated(upsertTeamEntry);
new AfterTeamDeleted(deleteTeamEntry);

new AfterFixtureLinkGenerated(upsertFixtureEntry);
new AfterFixtureUpdated(upsertFixtureEntry);
new AfterFixtureDeleted(deleteFixtureEntry);
