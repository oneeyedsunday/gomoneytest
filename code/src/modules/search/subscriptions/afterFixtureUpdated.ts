import { FixtureUpdated } from "@modules/fixtures/domain/events/fixtureUpdated";
import { DomainEvents } from "@shared/domain/events/DomainEvents";
import { IHandle } from "@shared/domain/events/IHandle";
import { LoggerImpl } from "@shared/infrastructure/logger";
import { UpsertFixtureEntry } from "../useCases/upsertFixtureEntry/upsertFixtureEntry";
import { PopulatedFixtureModel } from "@modules/fixtures/repos/models/Fixture";
import { SearchResultMap } from "../mappers/SearchResult";

export class AfterFixtureUpdated implements IHandle<FixtureUpdated> {
  private readonly upsertFixtureEntry: UpsertFixtureEntry;
  constructor(upsertFixtureEntry: UpsertFixtureEntry) {
    this.setupSubscriptions();
    this.upsertFixtureEntry = upsertFixtureEntry;
  }
  setupSubscriptions(): void {
    DomainEvents.register(
      (e, o) => this.onFixtureUpdated(e as FixtureUpdated, o as any),
      FixtureUpdated.name
    );
  }

  private async onFixtureUpdated(
    event: FixtureUpdated,
    options: Promise<PopulatedFixtureModel>
  ) {
    LoggerImpl.info(
      `[Search]: Received FixtureUpdated Event: ${event.getAggregateId()}`
    );
    const populatedFixture = await options;
    if (
      !populatedFixture ||
      !Array.isArray(populatedFixture?.teams) ||
      populatedFixture?.teams.length !== 2
    ) {
      LoggerImpl.info(
        "Fixture is invalid, teams could not be found. Skipping processing"
      );
      return;
    }
    const title = populatedFixture.teams.map((tM) => tM.name).join(" vs ");
    try {
      await this.upsertFixtureEntry.execute(
        SearchResultMap.toFixtureSearchResult(populatedFixture)
      );
      LoggerImpl.info(
        `[Search]: Updated fixture entry for #${event
          .getAggregateId()
          .toString()}: ${title}`
      );
    } catch (err) {
      LoggerImpl.error(
        `[Search]: Failed to update fixture entry for #${event
          .getAggregateId()
          .toString()}: ${title}`
      );
    }
  }
}
