import { TeamCreated } from "@modules/teams/domain/events/teamCreated";
import { DomainEvents } from "@shared/domain/events/DomainEvents";
import { IHandle } from "@shared/domain/events/IHandle";
import { LoggerImpl } from "@shared/infrastructure/logger";
import { TeamSearchResultData } from "../repos/models/SearchResult";
import { UpsertTeamEntry } from "../useCases/upsertTeamEntry/upsertTeamEntry";

export class AfterTeamCreated implements IHandle<TeamCreated> {
  private readonly upsertTeamEntry: UpsertTeamEntry;
  constructor(upsertTeamEntry: UpsertTeamEntry) {
    this.setupSubscriptions();
    this.upsertTeamEntry = upsertTeamEntry;
  }
  setupSubscriptions(): void {
    DomainEvents.register(
      (e) => this.onTeamCreated(e as TeamCreated),
      TeamCreated.name
    );
  }

  private async onTeamCreated(event: TeamCreated) {
    LoggerImpl.info(
      `[Search]: Received TeamCreated Event: ${event
        .getAggregateId()
        .toString()}`
    );

    try {
      const entry: TeamSearchResultData = {
        _id: event.team.id.toValue(),
        name: event.team.name.value,
        abbreviation: event.team.abbreviation.value,
      };
      await this.upsertTeamEntry.execute(entry);
      LoggerImpl.info(
        `[Search]: Added team entry for #${event
          .getAggregateId()
          .toString()}: ${event.team.name.value}`
      );
    } catch (err) {
      LoggerImpl.error(
        `[Search]: Failed to add team entry for #${event
          .getAggregateId()
          .toString()}: ${event.team.name.value}`
      );
    }
  }
}
