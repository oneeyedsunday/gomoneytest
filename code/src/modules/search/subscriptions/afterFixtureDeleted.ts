import { FixtureDeleted } from "@modules/fixtures/domain/events/fixtureDeleted";
import { DomainEvents } from "@shared/domain/events/DomainEvents";
import { IHandle } from "@shared/domain/events/IHandle";
import { LoggerImpl } from "@shared/infrastructure/logger";
import { FixtureSearchResultData } from "../repos/models/SearchResult";
import { DeleteFixtureEntry } from "../useCases/removeFixtureEntry/deleteFixtureEntry";

export class AfterFixtureDeleted implements IHandle<FixtureDeleted> {
  private readonly deleteFixtureEntry: DeleteFixtureEntry;
  constructor(deleteFixtureEntry: DeleteFixtureEntry) {
    this.setupSubscriptions();
    this.deleteFixtureEntry = deleteFixtureEntry;
  }
  setupSubscriptions(): void {
    DomainEvents.register(
      (e) => this.onFixtureDeleted(e as FixtureDeleted),
      FixtureDeleted.name
    );
  }

  private async onFixtureDeleted(event: FixtureDeleted) {
    LoggerImpl.info(
      `[Search]: Received FixtureDeleted Event: ${event.getAggregateId()}`
    );

    try {
      const entry = {
        _id: event.fixture.id.toValue(),
      } as unknown as FixtureSearchResultData;
      await this.deleteFixtureEntry.execute(entry);
      LoggerImpl.info(
        `[Search]: Deleted team entry for #${event.getAggregateId().toString()}`
      );
    } catch (err) {
      LoggerImpl.error(
        `[Search]: Failed to delete team entry for #${event
          .getAggregateId()
          .toString()}`
      );
    }
  }
}
