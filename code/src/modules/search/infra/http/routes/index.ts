import { getAllResultsController } from "@modules/search/useCases/broadSearch";
import { getFixtureResultsController } from "@modules/search/useCases/getFixtures";
import { getTeamResultsController } from "@modules/search/useCases/getTeams";
import express from "express";

const searchRouter = express.Router();

searchRouter.get("", (req, res) => {
  switch (((req.query.type as string) || "").toLowerCase()) {
    case "team":
      return getTeamResultsController.execute(req, res);
    case "fixture":
      return getFixtureResultsController.execute(req, res);
    default:
      return getAllResultsController.execute(req, res);
  }
});

export { searchRouter };
