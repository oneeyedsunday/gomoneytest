import { PopulatedFixtureModel } from "@modules/fixtures/repos/models/Fixture";
import { UniqueEntityID } from "@shared/domain/UniqueEntityID";
import { Mapper } from "@shared/infrastructure/Mapper";
import { SearchResultDTO } from "../dtos/searchResultDTO";
import { TeamResultDTO } from "../dtos/teamResultDTO";
import {
  FixtureSearchResultData,
  SearchResultModel,
  TeamSearchResultData,
} from "../repos/models/SearchResult";

export class SearchResultMap
  implements Mapper<SearchResultModel, SearchResultDTO>
{
  toDomain(raw: any): SearchResultModel {
    throw new Error("Method not implemented.");
  }
  toDTO(from: SearchResultModel): SearchResultDTO {
    return SearchResultMap.toDTO(from);
  }
  toDTOFromSerialized(raw: any): SearchResultDTO {
    throw new Error("Method not implemented.");
  }

  public static toDomain(from: any) {
    throw new Error("Method not implemented.");
  }

  public static toDTO(from: SearchResultModel): SearchResultDTO {
    const isTeam = from.type === "team";
    const isFixture = from.type === "fixture";
    return {
      resultType: from.type,
      team: isTeam
        ? {
            name: (from.data as TeamSearchResultData).name,
            abbreviation: (from.data as TeamSearchResultData).abbreviation,
            teamId: from.data._id?.toString(),
          }
        : undefined,
      fixture: isFixture
        ? {
            fixtureId: (from.data as FixtureSearchResultData)._id?.toString(),
            title: (from.data as FixtureSearchResultData).title,
            shortTitle: (from.data as FixtureSearchResultData).shortTitle,
            kickOff: (from.data as FixtureSearchResultData).date,
            link: (from.data as FixtureSearchResultData).link,
            teams: (from.data as FixtureSearchResultData).teams.map((tM) => ({
              name: (tM as TeamSearchResultData).name,
              abbreviation: (tM as TeamSearchResultData).abbreviation,
              teamId: tM._id?.toString(),
              isHomeTeam:
                ((from.data as FixtureSearchResultData)
                  .homeTeam as unknown as string) === tM._id,
            })) as unknown as [TeamResultDTO, TeamResultDTO],
          }
        : undefined,
    };
  }
  public static toDTOFromSerialized(raw: any): SearchResultDTO {
    throw new Error("Method not implemented.");
  }

  public static toFixtureSearchResult(
    fixture: PopulatedFixtureModel
  ): FixtureSearchResultData {
    const title = fixture.teams.map((tM) => tM.name).join(" vs ");
    const shortTitle = fixture.teams.map((tM) => tM.abbreviation).join(" vs ");

    return {
      _id: fixture._id,
      title,
      shortTitle,
      link: fixture.link || "",
      teams: fixture.teams.map((tM) => ({
        _id: new UniqueEntityID(tM._id).toValue(),
        name: tM.name,
        abbreviation: tM.abbreviation,
      })) as TeamSearchResultData[] as [
        TeamSearchResultData,
        TeamSearchResultData
      ],
      homeTeam: fixture.homeTeam ? fixture.homeTeam : undefined,
      date: new Date(fixture.startDate),
    };
  }
}
