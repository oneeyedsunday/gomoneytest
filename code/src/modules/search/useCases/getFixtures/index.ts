import { repo } from "../../repos";
import { GetFixtureResults } from "./GetFixtureResults";
import { GetFixtureResultsController } from "./GetFixtureResultsController";

const getFixtureResults = new GetFixtureResults(repo);
const getFixtureResultsController = new GetFixtureResultsController(
  getFixtureResults
);

export { getFixtureResults, getFixtureResultsController };
