import { SearchResultModel } from "@modules/search/repos/models/SearchResult";
import { ISearchRepo } from "@modules/search/repos/searchRepo";
import * as AppError from "@shared/core/AppError";
import { Either, Result, right, left } from "@shared/core/Result";
import { UseCase } from "@shared/core/UseCase";
import { ISearchOptions } from "@shared/domain/SearchOptions";

type Response = Either<AppError.UnexpectedError, Result<SearchResultModel[]>>;

export class GetFixtureResults
  implements UseCase<ISearchOptions, Promise<Response>>
{
  private repo: ISearchRepo;

  constructor(repo: ISearchRepo) {
    this.repo = repo;
  }

  public async execute(req: ISearchOptions): Promise<Response> {
    try {
      const results = await this.repo.getResults("fixture", req);
      return right(Result.ok<SearchResultModel[]>(results));
    } catch (err) {
      return left(new AppError.UnexpectedError(err));
    }
  }
}
