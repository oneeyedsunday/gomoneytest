import { TeamSearchResultData } from "@modules/search/repos/models/SearchResult";
import { ISearchRepo } from "@modules/search/repos/searchRepo";
import * as AppError from "@shared/core/AppError";
import { Either, Result, right, left } from "@shared/core/Result";
import { UseCase } from "@shared/core/UseCase";

type Response = Either<AppError.UnexpectedError, Result<void>>;

export class UpsertTeamEntry
  implements UseCase<TeamSearchResultData, Promise<Response>>
{
  private repo: ISearchRepo;

  constructor(repo: ISearchRepo) {
    this.repo = repo;
  }

  public async execute(req: TeamSearchResultData): Promise<Response> {
    try {
      await this.repo.upsertTeamResult(req);
      return right(Result.ok<void>());
    } catch (err) {
      return left(new AppError.UnexpectedError(err));
    }
  }
}
