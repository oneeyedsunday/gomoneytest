import { repo } from "../../repos";
import { UpsertTeamEntry } from "./upsertTeamEntry";

const upsertTeamEntry = new UpsertTeamEntry(repo);

export { upsertTeamEntry };
