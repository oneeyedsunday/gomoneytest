import { repo } from "../../repos";
import { GetTeamResults } from "./GetTeamResults";
import { GetTeamResultsController } from "./GetTeamResultsController";

const getTeamResults = new GetTeamResults(repo);
const getTeamResultsController = new GetTeamResultsController(getTeamResults);

export { getTeamResults, getTeamResultsController };
