import { repo } from "../../repos";
import { UpsertFixtureEntry } from "./upsertFixtureEntry";

const upsertFixtureEntry = new UpsertFixtureEntry(repo);

export { upsertFixtureEntry };
