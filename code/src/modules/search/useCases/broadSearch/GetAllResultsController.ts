import { BaseController } from "@shared/infrastructure/http/controllers/Base";
import { GetAllResults } from "./GetAllResults";
import express from "express";
import { ISearchOptions, SearchOptions } from "@shared/domain/SearchOptions";
import { SearchResultDTO } from "@modules/search/dtos/searchResultDTO";
import { SearchResultMap } from "@modules/search/mappers/SearchResult";

type ApiResponse = {
  results: SearchResultDTO[];
};

export class GetAllResultsController extends BaseController {
  private useCase: GetAllResults;

  constructor(useCase: GetAllResults) {
    super();
    this.useCase = useCase;
  }

  async executeImpl(
    req: express.Request,
    res: express.Response
  ): Promise<express.Response> {
    const dto: ISearchOptions = SearchOptions.FromObject(
      (req.query || {}) as any
    ) as unknown as ISearchOptions;

    try {
      const result = await this.useCase.execute(dto);

      if (result.isLeft()) {
        const error = result.value;

        switch (error.constructor) {
          default:
            return this.fail(res, error.errorValue().message);
        }
      } else {
        const results = result.value.getValue();
        return this.ok<ApiResponse>(res, {
          results: results.map((r) => SearchResultMap.toDTO(r)),
        });
      }
    } catch (err) {
      return this.fail(res, err);
    }
  }
}
