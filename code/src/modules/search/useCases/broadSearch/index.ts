import { repo } from "../../repos";
import { GetAllResults } from "./GetAllResults";
import { GetAllResultsController } from "./GetAllResultsController";

const getAllResults = new GetAllResults(repo);
const getAllResultsController = new GetAllResultsController(getAllResults);

export { getAllResults, getAllResultsController };
