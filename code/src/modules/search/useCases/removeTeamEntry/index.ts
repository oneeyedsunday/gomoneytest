import { repo } from "../../repos";
import { DeleteTeamEntry } from "./deleteTeamEntry";

const deleteTeamEntry = new DeleteTeamEntry(repo);

export { deleteTeamEntry };
