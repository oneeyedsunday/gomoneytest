import { repo } from "../../repos";
import { DeleteFixtureEntry } from "./deleteFixtureEntry";

const deleteFixtureEntry = new DeleteFixtureEntry(repo);

export { deleteFixtureEntry };
