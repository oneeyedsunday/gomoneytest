import { FixtureSearchResultData } from "@modules/search/repos/models/SearchResult";
import { ISearchRepo } from "@modules/search/repos/searchRepo";
import * as AppError from "@shared/core/AppError";
import { Either, Result, right, left } from "@shared/core/Result";
import { UseCase } from "@shared/core/UseCase";
import { UniqueEntityID } from "@shared/domain/UniqueEntityID";

type Response = Either<AppError.UnexpectedError, Result<void>>;
type Request = FixtureSearchResultData;

export class DeleteFixtureEntry implements UseCase<Request, Promise<Response>> {
  private repo: ISearchRepo;

  constructor(repo: ISearchRepo) {
    this.repo = repo;
  }

  public async execute(req: Request): Promise<Response> {
    try {
      await this.repo.removeFixtureResult(new UniqueEntityID(req._id));
      return right(Result.ok<void>());
    } catch (err) {
      return left(new AppError.UnexpectedError(err));
    }
  }
}
