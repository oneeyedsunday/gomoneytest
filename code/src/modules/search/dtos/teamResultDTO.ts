export interface TeamResultDTO {
  name: string;
  abbreviation: string;
  teamId?: string;
  isHomeTeam?: boolean;
}
