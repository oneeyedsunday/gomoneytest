import { TeamResultDTO } from "./teamResultDTO";

export interface FixtureResultDTO {
  title: string;
  shortTitle?: string;
  kickOff: Date;
  link: string | null;
  fixtureId: string;
  teams: [TeamResultDTO, TeamResultDTO];
}
