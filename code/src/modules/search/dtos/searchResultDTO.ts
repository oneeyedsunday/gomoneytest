import { SearchResultType } from "../repos/models/SearchResult";
import { FixtureResultDTO } from "./fixtureResultDTO";
import { TeamResultDTO } from "./teamResultDTO";

export interface SearchResultDTO {
  resultType: SearchResultType;
  team?: TeamResultDTO;
  fixture?: FixtureResultDTO;
}
