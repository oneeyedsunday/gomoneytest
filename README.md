# GoMoneyTest

Repo for GoMoney backend dev test

### Postman Runner
Postman Live Runner [here](https://www.postman.com/oneeyedsunday/workspace/3be773f1-a058-4840-82f2-0fd8a3d43299/collection/4942008-c07c98c4-8a0b-4294-8c75-975685373cb3?ctx=documentation)

### Postman Documentation
See documentation [here](https://documenter.getpostman.com/view/4942008/TzRXARrv)

### Hoste API
Base Endpoint [https://nameless-woodland-12567.herokuapp.com/api]
