### User Stories
## Teams
- Admin should be able to create team
- Admin should be able to edit team
- Admin should be able to delete team
- Admin / User should be able to get team
- Admin / User should be able to list teams
## Fixtures
- Admin should be able to create fixture
- Admin should be able to generate unique link for fixture
- Admin should be able to edit fixture
- Admin should be able to delete fixture
- Admin / User should be able to get fixture
- Admin / User should be able to list fixtures
- Admin / User should be able to list / get completed fixture(s)
- Admin / User should be able to list / get pending fixture(s)
- Everyone (including anon access) should be able to robustly search fixtures/teams
## Auth
- A user should be able to sign up
- A user should be able to sign in
- An admin (special user type) should be able to sign in
- An admin (will be made via some backoffice process)



