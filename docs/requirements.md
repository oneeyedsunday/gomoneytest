### Design Doc

## What we are building
API that serves the latest scores of fixtures of matches in a “Mock Premier League”

## Requirements
- Signup / Login (Admin, User accounts)
- Manage teams (CRUD, Admin only, User is read only)
- Manage fixtures (CRUD, Admin only, User is view only)
- Generate Unique Links for Fixtures
- Robust Search for fixtures/teams (full text searches?, public)


## Others
Auth via Session + Jwt

## Constraints
- NodeJs (Typescript + ExpressJs) or Golang
- MongoDB (primary persistence)
- Redis (cache)
- Docker
- POSTMAN (docs, etc)



## Submission Checklist
- Deploy
- E2E tests for user stories
- Cache APIs
- Rate limit Auth guarded routes
- Pagination

