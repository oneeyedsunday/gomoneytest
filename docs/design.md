### High Level Overview
Our API will use 
- Seek to implement the [JSend](https://github.com/omniti-labs/jsend) specification for serving responses. ([see](https://github.com/random-guys/jSend))
- Use [this](https://github.com/jirevwe/raydis-js) for a full promise based redis connection 
- Handle Authentication and Authorization via `JWT` viz `Bearer Token` format [a candidate is [this jwt lib](https://github.com/random-guys/jwt256)]
- Handle mongoose crud via this [opinionated Repository pattern implementation](https://github.com/random-guys/bucket)
- For full text search, we may use elastisearch, emit events when fixtures are created (and / or links generated), consume these events and generated records. [EventBus](https://github.com/random-guys/eventbus) is a viable wrapper, but with time constraints, we may use an inmemory implementation.
- Logging will be implemented (TBD), but intend to constrain to the usual interface (`error`, `info`, `trace`...)
- Document API with Postman, see [jusibe](https://jusibe.com/docs/), [example](https://app.getpostman.com/view-collection/7860bb0385d0899384b4?referrer=https%3A%2F%2Fjusibe.com%2Fdocs) 


### Design 
I'm tending towards CQRS + DDD for this, so far, most of what I've worked with has been the imperative MVC.
Why CQRS? Well, as I already envisage with the searching, I want to leverage ELastisearch,
For now, the plan is to emit events, and handle them. WHat does this mean for the search, create (optionally, edit) a fixture,
fire up an event, handle it by persistence and indexing (to ES).
_This will likely not follow full DDD_

For signup / signin, it would make sense to offload to firebase as against building that out myself,
plus time constraints (I really want to submit this before Monday 17th). Besides, there's nothing extraordinary we're doing with the auth.
Only tricky thing is storing Role details in firebase.


### Testing
Usually, i do bdd with the test files in a folder with mocha.
Recently, I've been seeing lots of `file`, `file.spec` in action, it does feel like its a good way to promote unit testing a bit closer to the logic.
I'd like to run the app in TDD fashion, define the tests first before the logic, but the architecture is a tad new territory to me,
so we'd have a blend of unit tests with dependencies mocked and some integration tests for the api.

