### Models
## User
... the usual bare metal user props
Since we have admins and normal users, we need to track this.
We will compute an `isAdmin` property on the User model
## Team
Name
Nonce

Nonce  will be computed from team name on creation.
It will be used to generate unique links in fixtures

## Fixture
Title
Link - should be unique and readonly
StartTime - time fixture is set to start
Status (computed from startTime) - whether fixture has started, completed, is ongoing
Teams - Teams involved in fixture
Home Team (nullable) - Which team is (home) where applicable 

### Misc
Teams and fixtures will have timestamps
